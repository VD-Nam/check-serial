<?php

namespace App\Imports;

use App\Product;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class productImport implements  ToCollection
{
    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $k => $row)
        {
            if ($k >= 3 && isset($row[0]) && isset($row[3]) && isset($row[6])) {
                Product::create([
                    'name'     => trim($row[0]),
                    'category'    => trim($row[3]),
                    'trademark' => trim($row[6])
                    ]);
            }
        }
    }
}
