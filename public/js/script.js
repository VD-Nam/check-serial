(function($){
	"use strict";
	$(document).ready(function(){

		$('.panel-heading').click(function(){
			$(this).find('.panel-title').find("a")
				.toggleClass('glyphicon-down glyphicon-up');
		});


    		$('.custom-Canvas').each(function() {
			var canvas = $(this).get(0);;
            var context = canvas.getContext('2d');

            context.beginPath();
            context.moveTo(0, 0);
            context.lineTo(200, 0);
            context.lineWidth = 4;
            context.strokeStyle = '#009CFF';
            context.lineCap = 'butt';
            context.stroke();
		})

		/* Scroll to top */
		pogon_scrollUp();
		function pogon_scrollUp(options) {

			var defaults = {
				scrollName: 'scrollUp',
				topDistance: 600,
				topSpeed: 800,
				animation: 'fade',
				animationInSpeed: 200,
				animationOutSpeed: 200,
				scrollText: '<i class="fas fa-angle-up"></i>',
				scrollImg: false,
				activeOverlay: false
			};

			var o = $.extend({}, defaults, options),
			scrollId = '#' + o.scrollName;


			$('<a/>', {
				id: o.scrollName,
				href: '#top',
				title: o.scrollText
			}).appendTo('body');


			if (!o.scrollImg) {

				$(scrollId).html(o.scrollText);
			}


			$(scrollId).css({'display': 'none', 'position': 'fixed', 'z-index': '2147483647'});


			if (o.activeOverlay) {
				$("body").append("<div id='" + o.scrollName + "-active'></div>");
				$(scrollId + "-active").css({'position': 'absolute', 'top': o.topDistance + 'px', 'width': '100%', 'border-top': '1px dotted ' + o.activeOverlay, 'z-index': '2147483647'});
			}


			$(window).scroll(function () {
				switch (o.animation) {
					case "fade":
					$(($(window).scrollTop() > o.topDistance) ? $(scrollId).fadeIn(o.animationInSpeed) : $(scrollId).fadeOut(o.animationOutSpeed));
					break;
					case "slide":
					$(($(window).scrollTop() > o.topDistance) ? $(scrollId).slideDown(o.animationInSpeed) : $(scrollId).slideUp(o.animationOutSpeed));
					break;
					default:
					$(($(window).scrollTop() > o.topDistance) ? $(scrollId).show(0) : $(scrollId).hide(0));
				}
			});


			$(scrollId).on( "click", function (event) {
				$('html, body').animate({scrollTop: 0}, o.topSpeed);
				event.preventDefault();
			});

		}

		/* Fix empty menu in test_uni_data */
		if( $( '.widget_nav_menu ul li' ).length > 0 ){
			$( '.widget_nav_menu ul li a:empty' ).parent().css('display','none');
		}


		/* Stick Menu When Scroll Page */
		function pogon_scroll(){
			if( $('.ovamenu_shrink').length > 0 ){

				var header = $('.ovamenu_shrink');
				var scroll = $(window).scrollTop();

				if (scroll >= 250) {
					header.addClass('active_fixed');
				} else{
					header.removeClass('active_fixed');
				}

			}
		}
		$(window).scroll(function () { pogon_scroll(); });

		/* Popup Image - PrettyPhoto */
		if( $("a[data-gal^='prettyPhoto']").length > 0 ){
			$("a[data-gal^='prettyPhoto']").prettyPhoto({hook: 'data-gal', theme: 'facebook',slideshow:5000, autoplay_slideshow:true});
		}


		$( '.ovatheme_header_default li.menu-item button.dropdown-toggle').off('click').on( 'click', function() {
			$(this).parent().toggleClass('active_sub');
		});

		$(".categories a").siblings('i.fa-bookmark').css('display', 'inline-block');

		//delete character special of counter number
		$(".pogon_counter .elementor-counter-number").attr('data-delimiter','');

		/* Caousel Thumbnail Woo */
		if( $('.woo-thumbnails').length > 0 ){
			$('.woo-thumbnails .owl-carousel').each(function(){

				var rtl = false;
				if( $('body').hasClass('rtl') ){
					rtl = true;
				}

				$(this).owlCarousel({
					autoplay: true,
					autoplayHoverPause: true,
					loop: false,
					margin: 20,
					dots: false,
					nav: true,
					vertical: true,
					rtl: rtl,
					responsive: {
						0:    {items: 2},
						479:  {items: 2},
						768:  {items: 3},
						1024: {items: 3}
					}
				});
			});
		}

	    //heading
	    $( ".myCanvas" ).each(function( index ) {
	    	var canvas = $(this).get(0);
	    	var context = canvas.getContext("2d");

	    	var color = $(this).attr('data-color');

	    	var startX = 10;
	    	var startY = 6;
	    	var zigzagSpacing = 7.5;
	    	context.lineWidth = 4;
	    	context.strokeStyle = color;
	    	context.beginPath();
	    	context.moveTo(startX, startY);

	    	for (var n = 0; n < 9; n++) {
	    		var x = startX + ((n -1) * zigzagSpacing);
	    		var y;

          if (n % 2 == 0) { // if n is even...
          	y = startY + 8;
          }
          else { // if n is odd...
          	y = startY;
          }
          context.lineTo(x, y);
      }

      context.stroke();
  });


	});

})(jQuery);
