(function($){
	"use strict";
	

	$(window).on('elementor/frontend/init', function () {

		
		/* Menu Shrink */
    elementorFrontend.hooks.addAction('frontend/element_ready/ova_menu.default', function(){

     $( '.ova_menu_clasic .ova_openNav' ).on( 'click', function(){
      $( this ).closest('.ova_wrap_nav').find( '.ova_nav' ).removeClass( 'hide' );
      $( this ).closest('.ova_wrap_nav').find( '.ova_nav' ).addClass( 'show' );
      $( '.ova_menu_clasic  .ova_closeCanvas' ).css( 'width', '100%' );


      $( 'body' ).css( 'background-color', 'rgba(0,0,0,0.4)' );

    });

     $( '.ova_menu_clasic  .ova_closeNav' ).on( 'click', function(){
      $( this ).closest('.ova_wrap_nav').find( '.ova_nav' ).removeClass( 'show' );
      $( this ).closest('.ova_wrap_nav').find( '.ova_nav' ).addClass( 'hide' );
      $( '.ova_closeCanvas' ).css( 'width', '0' );



      $( 'body' ).css( 'background-color', 'transparent' );

    });

  			// Display in mobile
  			$( '.ova_menu_clasic li.menu-item button.dropdown-toggle').off('click').on( 'click', function() {
         $(this).parent().toggleClass('active_sub');
       });

       $(window).scroll(function () {
        if( $('.ovamenu_shrink').length > 0 ){

         var header = $('.ovamenu_shrink');
         var scroll = $(window).scrollTop();

         if (scroll >= 200) {
          header.addClass('ready_active');
        }else{
          header.removeClass('ready_active');
        }
        if (scroll >= 250) {
          header.addClass('active_fixed');
        } else{
         header.removeClass('active_fixed');
       }

     }

   });
     });

    /* Menu Henbergar */
    elementorFrontend.hooks.addAction('frontend/element_ready/henbergar_menu.default', function(){

     $( '.ova_menu_canvas .ova_openNav' ).on( 'click', function(){
      $( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).removeClass( 'hide' );
      $( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).addClass( 'show' );
      $( '.ova_menu_canvas .ova_closeCanvas' ).css( 'width', '100%' );

      $( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).addClass('active');


      $( 'body' ).css( 'background-color', 'rgba(0,0,0,0.2)' );

    });

     $( '.ova_menu_canvas .ova_closeNav' ).on( 'click', function(){
      $( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).removeClass( 'show' );
      $( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).addClass( 'hide' );
      $( '.ova_menu_canvas .ova_closeCanvas' ).css( 'width', '0' );

      $( this ).closest('.ova_wrap_nav').find( '.ova_nav_canvas' ).removeClass('active');



      $( 'body' ).css( 'background-color', 'transparent' );

    });

       // Display in mobile
       $( '.ova_menu_canvas li.menu-item button.dropdown-toggle').off('click').on( 'click', function() {
        $(this).parent().toggleClass('active_sub');
      });
       

       $(window).scroll(function () {
        if( $('.ovamenu_shrink').length > 0 ){

         var header = $('.ovamenu_shrink');
         var scroll = $(window).scrollTop();

         if (scroll >= 200) {
          header.addClass('ready_active');
        }else{
          header.removeClass('ready_active');
        }
        if (scroll >= 250) {
          header.addClass('active_fixed');
        } else{
          header.removeClass('active_fixed');
        }

      }

    });

     });
    /* end Menu Henbergar */

    /* Search Popup */
    elementorFrontend.hooks.addAction('frontend/element_ready/ova_search.default', function(){
     $( '.wrap_search_pogon_popup i' ).on( 'click', function(){
      $( this ).closest( '.wrap_search_pogon_popup' ).addClass( 'show' );
    });

     $( '.btn_close' ).on( 'click', function(){
      $( this ).closest( '.wrap_search_pogon_popup' ).removeClass( 'show' );

    });
   });
    /* end Search Popup */

    /* Slide Show */  
    elementorFrontend.hooks.addAction('frontend/element_ready/ova_banner_slideshow.default', function(){

      function fadeInReset(element) {
        $(element).find('*[data-animation]').each(function(){
          var animation = $(this).data( 'animation' );
          $(this).removeClass( 'animated' );
          $(this).removeClass( animation );
          $(this).css({ opacity: 0 });
        });
      }

      function fadeIn(element) {

        // Sub Title
        var $title = $(element).find( '.active .elementor-slide-subtitle' )
        var animation_title = $title.data( 'animation' );
        var duration_title  = parseInt( $title.data( 'animation_dur' ) );
        

        setTimeout(function(){
          $title.addClass(animation_title).addClass('animated').css({ opacity: 1 });
        }, duration_title);


        /* Title */
        var $sub_title = $(element).find( '.active .elementor-slide-title' );
        var animation_sub_title = $sub_title.data( 'animation' );
        var duration_sub_title  = parseInt( $sub_title.data( 'animation_dur' ) );


        setTimeout(function(){
         $sub_title.addClass(animation_sub_title).addClass('animated').css({ opacity: 1 });
       }, duration_sub_title);

        /* Description */
        var $desc = $(element).find( '.active .elementor-slide-description' );
        var animation_desc = $desc.data( 'animation' );
        var duration_desc  = parseInt( $desc.data( 'animation_dur' ) );


        setTimeout(function(){
         $desc.addClass(animation_desc).addClass('animated').css({ opacity: 1 });
       }, duration_desc);

        /* Button */
        var $button = $(element).find( '.active .elementor-slide-button' );
        var animation_button = $button.data( 'animation' );
        var duration_button  = parseInt( $button.data( 'animation_dur' ) );

        setTimeout(function(){
         $button.addClass(animation_button).addClass('animated').css({ opacity: 1 });
       }, duration_button);


      }

      $(document).ready(function(){

        $('.elementor-slides').each(function(){

          var owl = $(this);
          var data = owl.data("owl_carousel");

          owl.on('initialized.owl.carousel', function(event) {

            fadeIn(event.target);

          });

          owl.owlCarousel(
            data
            );

          owl.on('translate.owl.carousel', function(event){
            fadeInReset(event.target);
            owl.trigger('stop.owl.autoplay');
            owl.trigger('play.owl.autoplay');
          });

          owl.on('translated.owl.carousel', function(event) {
            fadeIn(event.target);
            owl.trigger('stop.owl.autoplay');
            owl.trigger('play.owl.autoplay');
          });

        });

      });

      $(function() {
        $('.scroll-btn a[href*=#]').on('click', function(e) {
          e.preventDefault();
          $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top}, 1000, 'linear');
        });
      });

    });
    /* End Slide Show */

    /* price table  tab */
    elementorFrontend.hooks.addAction('frontend/element_ready/ova_price_tab.default', function(){
      var id_first = $('.ova-tab-price .tab-price button:first-child').addClass('active').attr('id');
      $('.price-tab.'+id_first).fadeIn( 100 );
      $('.ova-tab-price .tab-price button').off('click').on( 'click', function(){
        var id = $(this).attr('id');
        $('.ova-tab-price .tab-price button').removeClass('active');
        $(this).addClass('active');
        $('.price-tab').fadeOut( 10 );
        $('.price-tab.'+id).fadeIn( 500 );

      });


    });
    /* end price table  tab */

    /* price table  tab */
    elementorFrontend.hooks.addAction('frontend/element_ready/ova_our_skill.default', function(){


      $( ".circle" ).each(function( index ) {
        var circle = $(this);
        var size_value = circle.attr('data-size');
        var color_value = circle.attr('data-color');
        var color_empty = circle.attr('data-empty');
        $(circle).circleProgress({
         value: 0,
         size: size_value,
         emptyFill: color_empty,
       });

      });

      var circles = $('.circle');

      circles.appear({ force_process: true });

      circles.on('appear', function() {
        var circle = $(this);
        var number_value = parseInt(circle.attr('data-number'));
        console.log(number_value);
        var size_value = circle.attr('data-size');
        var color_value = circle.attr('data-color');
        var color_empty = circle.attr('data-empty');
        var time = parseInt(circle.attr('data-time'));
        if (!circle.data('inited')) {
          circle.circleProgress({
            value: (number_value) / 100,
            size: size_value,
            fill: {color:color_value},
            emptyFill: color_empty,
            animation: { duration: time},
          }).on('circle-animation-progress', function(event, progress, stepValue) {
            $(this).find('strong').html(Math.round(100 * stepValue) + '%');
          });
          circle.data('inited', true);
        }
      });

    });
    /* end price table  tab */

    /* Testimonial Slide*/

    elementorFrontend.hooks.addAction('frontend/element_ready/ova_testimonial.default', function(){

      $(".slide-testimonials").each(function(){
        var owlsl = $(this) ;
        var owlsl_ops = owlsl.data('options') ? owlsl.data('options') : {};
        if ($('.slide-testimonials').hasClass('version_1')) {
          var responsive_value = {
            0:{
              items:1,
              nav:false
            },
            576:{
              items:1

            },
            992:{
              items:2
            },
            1170:{
              items:2
            }
          };
        } 

        if ($('.slide-testimonials').hasClass('version_2')) {
          var responsive_value = {
            0:{
              nav:false
            },
            1170:{
              items:1
            }
          };
        } 
        
        owlsl.owlCarousel({
          autoWidth: owlsl_ops.autoWidth,
          margin: owlsl_ops.margin,
          items: owlsl_ops.items,
          loop: owlsl_ops.loop,
          autoplay: owlsl_ops.autoplay,
          autoplayTimeout: owlsl_ops.autoplayTimeout,
          center: owlsl_ops.center,
          nav: owlsl_ops.nav,
          dots: owlsl_ops.dots,
          thumbs: owlsl_ops.thumbs,
          autoplayHoverPause: owlsl_ops.autoplayHoverPause,
          slideBy: owlsl_ops.slideBy,
          smartSpeed: owlsl_ops.smartSpeed,
          navText:[
          '<i class="fa fa-angle-left" ></i>',
          '<i class="fa fa-angle-right" ></i>'
          ],
          responsive: responsive_value,
        });

      });

    });

    /* End Testimonial*/

    /* Video Popup */

    elementorFrontend.hooks.addAction('frontend/element_ready/video_popup.default', function(){

      $(document).ready( function(){
        $("#video").videoPopup({
          autoplay: 1,
          controlsColor: 'white',
          showVideoInformations: 0,
          width: 1000,
          customOptions: {
           rel: 0,
           end: 60
         }
       });
      });

    });

    /* End Video Popup*/

    /*  */

    elementorFrontend.hooks.addAction('frontend/element_ready/ova_heading.default', function(){  



     $( ".myCanvas" ).each(function( index ) {
      var canvas = $(this).get(0);
      var context = canvas.getContext("2d");

      var color = $(this).attr('data-color');

      var startX = 10;
      var startY = 6;
      var zigzagSpacing = 7.5;
      context.lineWidth = 4;
      context.strokeStyle = color;
      context.beginPath();
      context.moveTo(startX, startY);

      for (var n = 0; n < 9; n++) {
        var x = startX + ((n -1) * zigzagSpacing);
        var y;

          if (n % 2 == 0) { // if n is even...
            y = startY + 8;
          }
          else { // if n is odd...
            y = startY;
          }
          context.lineTo(x, y);
        }

        context.stroke();
      });



   });

    /* End Video Popup*/


  });

})(jQuery);
