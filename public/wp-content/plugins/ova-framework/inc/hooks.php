<?php

class ovaframework_hooks {

	public function __construct() {
		
		// Customize Menu Structures
		add_filter( 'wp_nav_menu_args', array( $this, 'ova_prefix_modify_nav_menu_args' ) );
		
		// Share Social in Single Post
		add_filter( 'ova_share_social', array( $this, 'pogon_content_social' ), 2, 10 );

		// Allow add font class to title of widget
		add_filter( 'widget_title', array( $this, 'ova_html_widget_title' ) );
		

		add_filter( 'widget_text', 'do_shortcode' );


		add_filter( 'upload_mimes', array( $this, 'upload_mimes' ), 1, 1);


		add_action( 'woocommerce_single_product_summary', array( $this, 'ova_share_social_woo_single' ), 41 );


    }

    /* Filter Walker for all menu */
	public function ova_prefix_modify_nav_menu_args( $args ) {
		return array_merge( $args, array(
				'fallback_cb' => 'Ova_Walker_Menu::fallback',
				'walker'      => new Ova_Walker_Menu()
		    ) );
	}

	public function pogon_content_social( $link, $title ) {
 		$html = '<ul class="share-social-icons clearfix">
			
			<li><a class="share-ico ico-facebook" target="_blank" href="http://www.facebook.com/sharer.php?u='.$link.'"><span>'.esc_html("Facebook", "ova-framework").'</span><i class="fa fa-facebook"></i></a></li>
			
			<li><a class="share-ico ico-twitter" target="_blank" href="https://twitter.com/share?url='.$link.'&amp;text='.urlencode($title).'&amp;hashtags=simplesharebuttons"><span>'.esc_html("Twitter", "ova-framework").'</span><i class="fab fa-twitter"></i></a></li>
						
			<li><a class="share-ico ico-pinterest" target="_blank" href="http://www.pinterest.com/pin/create/button/?url='.$link.'"><span>'.esc_html("Pinterest", "ova-framework").'</span><i class="fa fa-pinterest-p" ></i></a></li>
			<li><a class="share-ico ico-mail" href="mailto:?body='.$link.'"><span>'.esc_html("Email", "ova-framework").'</span><i class="fa fa-envelope-o"></i></a></li>                            
			
			
		</ul>';
		return $html;
 	}


	// Filter class in widget title
	public function ova_html_widget_title( $title ) {
		$title = str_replace( '{{', '<i class="', $title );
		$title = str_replace( '}}', '"></i>', $title );
		return $title;
	}

	// Supports SVg+xml

	public function upload_mimes($mimes){
		$mimes['zip'] = 'application/zip';
		$mimes['7z']  = 'application/x-7z-compressed';
		$mimes['apk'] = 'application/apk';
		$mimes['psd'] = 'image/vnd.adobe.photoshop';
		$mimes['rar'] = 'application/x-rar-compressed';
		$mimes['swf'] = 'application/x-shockwave-flash';
		$mimes['exe'] = 'application/x-msdownload';
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}
	
	// Share Social Single Product

	public function ova_share_social_woo_single(){

 	?>
	    <div class="share_social">
	        <i class="fa fa-share-alt"></i>
	        <span class="ova_label"><?php esc_html_e('Share', 'ova-framework'); ?></span>
	        <?php echo apply_filters('ova_share_social', get_the_permalink(), get_the_title() ); ?>
	    </div>
	<?php
	 
	}

}

new ovaframework_hooks();

