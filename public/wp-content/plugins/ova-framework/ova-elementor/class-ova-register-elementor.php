<?php

namespace ova_framework;

use ova_framework\widgets\ova_menu;
use ova_framework\widgets\ova_logo;
use ova_framework\widgets\ova_header_breadcrumbs;
use ova_framework\widgets\ova_search;
use ova_framework\widgets\ova_social;
use ova_framework\widgets\henbergar_menu;
use ova_framework\widgets\ova_icon_pogon;
use ova_framework\widgets\ova_blog_footer;
use ova_framework\widgets\ova_heading;
use ova_framework\widgets\ova_banner_slideshow;
use ova_framework\widgets\ova_price_table;
use ova_framework\widgets\ova_price_tab;
use ova_framework\widgets\ova_blog;
use ova_framework\widgets\ova_service;
use ova_framework\widgets\ova_about_team;
use ova_framework\widgets\ova_our_skill;
use ova_framework\widgets\ova_contact;

use ova_framework\widgets\ova_testimonial;
use ova_framework\widgets\video_popup;
use ova_framework\widgets\ova_contact_info;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly




/**
 * Main Plugin Class
 *
 * Register new elementor widget.
 *
 * @since 1.0.0
 */
class Ova_Register_Elementor {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function __construct() {
		$this->add_actions();
	}

	/**
	 * Add Actions
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function add_actions() {

		// Register Header Footer Category in Pane
	    add_action( 'elementor/elements/categories_registered', array( $this, 'add_hf_category' ) );

	     // Register Ovatheme Category in Pane
	    add_action( 'elementor/elements/categories_registered', array( $this, 'add_ovatheme_category' ) );
	    
		
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'on_widgets_registered' ] );
		

	}

	
	public  function add_hf_category(  ) {
	    \Elementor\Plugin::instance()->elements_manager->add_category(
	        'hf',
	        [
	            'title' => __( 'Header Footer', 'ova-framework' ),
	            'icon' => 'fa fa-plug',
	        ]
	    );

	}

	
	public function add_ovatheme_category(  ) {

	    \Elementor\Plugin::instance()->elements_manager->add_category(
	        'ovatheme',
	        [
	            'title' => __( 'Ovatheme', 'ova-framework' ),
	            'icon' => 'fa fa-plug',
	        ]
	    );

	}


	/**
	 * On Widgets Registered
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 */
	public function on_widgets_registered() {
		$this->includes();
		$this->register_widget();
	}

	/**
	 * Includes
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function includes() {
		
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-menu.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-logo.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-header-breabcrumbs.php';

		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-search.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-social.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-henbergar_menu.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-icon_pogon.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-blog-footer.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova_heading.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-banner-slide.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova_price_table.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova_price_tab.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova_blog.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova_service.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova_about_team.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova_our_skill.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova_contact.php';

		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-testimonial.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-video-popup.php';
		require OVA_PLUGIN_PATH . 'ova-elementor/widgets/ova-contact-info.php';
		
	}

	/**
	 * Register Widget
	 *
	 * @since 1.0.0
	 *
	 * @access private
	 */
	private function register_widget() {

		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_menu() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_logo() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_header_breadcrumbs() );

		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_search() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_social() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new henbergar_menu() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_icon_pogon() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_blog_footer() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_heading() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_banner_slideshow() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_price_table() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_price_tab() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_blog() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_service() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_about_team() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_our_skill() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_contact() );

		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_testimonial() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new video_popup() );
		\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new ova_contact_info() );
		
	}
	    
	

}

new Ova_Register_Elementor();





