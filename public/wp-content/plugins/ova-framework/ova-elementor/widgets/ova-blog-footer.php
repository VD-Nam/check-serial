<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_blog_footer extends Widget_Base {

	public function get_name() {
		return 'ova_blog_footer';
	}

	public function get_title() {
		return __( 'Latest News', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-posts-ticker';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function _register_controls() {

		$args = array(
			'orderby' => 'name',
			'order'   => 'ASC'
		);

		$categories   =get_categories($args);
		$cate_array   = array();
		$arrayCateAll = array( 'all' => 'All categories ' );
		if ($categories) {
			foreach ( $categories as $cate ) {
				$cate_array[$cate->cat_name] = $cate->slug;
			}
		} else {
			$cate_array["No content Category found"] = 0;
		}



		//SECTION CONTENT
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$this->add_control(
			'category',
			[
				'label'   => __( 'Category', 'ova-framework' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'all',
				'options' => array_merge($arrayCateAll,$cate_array),
			]
		);

		$this->add_control(
			'total_count',
			[
				'label'   => __( 'Total Post', 'ova-framework' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3,
			]
		);

		$this->add_control(
			'order_by',
			[
				'label'   => __('Order By', 'ova-framework'),
				'type'    => Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => [
					'asc'  => __('ASC', 'ova-framework'),
					'desc' => __('DESC', 'ova-framework'),
				]
			]
		);

		$this->add_control(
			'number_title',
			[
				'label'   => __( 'Number Word Title', 'ova-framework' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 6,
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .pogon-blog .post_items .post_content h4',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_title',
			[
				'label' => __( 'Color Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .pogon-blog .post_items .post_content h4 a' => 'color : {{VALUE}};border-color : {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'color_title_hover',
			[
				'label' => __( 'Color Title Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .pogon-blog .post_items .post_content h4 a:hover' => 'color : {{VALUE}};border-color : {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_date',
			[
				'label' => __( 'Post Date', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'date_typography',
				'selector' => '{{WRAPPER}} .pogon-blog .post_items .post_content .post-date',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_date',
			[
				'label' => __( 'Date Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .pogon-blog .post_items .post_content .post-date' => 'color : {{VALUE}};border-color : {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

	}


	protected function render() {
		$settings = $this->get_settings_for_display();
		
		$category     = $settings['category'];
		$total_count  = $settings['total_count'];
		$order        = $settings['order_by'];
		
		$number_title = $settings['number_title'] ? $settings['number_title'] : 8;

		$args = [];
		if ($category == 'all') {
			$args = [
				'post_type'      => 'post',
				'posts_per_page' => $total_count,
				'order'          => $order,
			];
		} else {
			$args = [
				'post_type'      => 'post', 
				'category_name'  => $category,
				'posts_per_page' => $total_count,
				'order'          => $order,
			];
		}

		$post_query = new \WP_Query($args);

		?>
		<div class="pogon-blog">

			<?php
			if( $post_query->have_posts() ) : while( $post_query->have_posts() ) : $post_query->the_post();

				$img_src = wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' );
				$img_srcset = wp_get_attachment_image_srcset( get_post_thumbnail_id(), 'medium' );

				?>

				<div class="post_items">
					<?php if( $img_src != '' ) : ?>
					<div class="img-feature">							
						<img src="<?php echo $img_src;?>" srcset="<?php echo $img_srcset;?>" sizes="(max-width: 600px) 100vw, 600px" alt="<?php the_title();?>">
					</div>
					<?php endif; ?>
					<div class="post_content">
						<h4><a href="<?php echo get_the_permalink(); ?>" title="<?php echo get_the_title();?>"><?php echo esc_html(pogon_custom_text(get_the_title(), $number_title)); ?></a></h4>
						<span class="post-date">
							<?php the_time(get_option('date_format')); ?>
						</span>
					</div>
				</div>

				<?php
			endwhile; endif; wp_reset_postdata();
			?>

		</div>
		<?php
	}
}
