<?php
namespace ova_framework\Widgets;
use Elementor;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Group_Control_Typography;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class ova_contact_info extends Widget_Base {

	public function get_name() {
		return 'ova_contact_info';
	}

	public function get_title() {
		return __( 'Contact Info', 'ova-framework' );
	}

	public function get_icon() {
		return 'fa fa-mobile';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_keywords() {
		return [ 'contact', 'address', 'us' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_contact_us',
			[
				'label' => __( 'Contact Us', 'ova-framework' ),
			]
		);
			$this->add_control(
				'heading_icon',
				[
					'label' => __( 'Icon', 'ova-framework' ),
					'type' => \Elementor\Controls_Manager::HEADING,
				]
			);

			$this->add_control(
				'icon',
				[
					'label' => __( 'Choice Icons', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::ICON,
					'default' => 'fa fa-facebook',
				]
			);

			$this->add_control(
				'icon_class',
				[
					'label' => __( 'Class Icons', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::TEXT,
					'placeholder' => __( 'zmdi zmdi-pin', 'ova-framework' ),
				]
			);

			$this->add_control(
				'color_icon',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova_contact_us i' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'hover_color_icon',
				[
					'label' => __( 'Hover Color', 'ova-framework' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova_contact_us i:hover' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'size_icon',
				[
					'label' => __( 'Size', 'plugin-domain' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px', '%' ],
					'range' => [
						'px' => [
							'min' => 5,
							'max' => 1000,
						],
						'%' => [
							'min' => 0,
							'max' => 100,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .ova_contact_us .icon' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'icon_margin',
				[
					'label' => __( 'Margin', 'ova-framework' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .ova_contact_us .icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_control(
				'heading_text',
				[
					'label' => __( 'Text', 'ova-framework' ),
					'type' => \Elementor\Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'text',
				[
					'type' => \Elementor\Controls_Manager::TEXT,
					'placeholder' => __( 'Text', 'ova-framework' ),
					'label_block' => true,
					'default' => __( '(+88) 12 345 6789', 'ova-framework' ),
				]
			);

			$this->add_control(
			'contact_link',
				[
					'label' => __( 'Link', 'plugin-domain' ),
					'type' => \Elementor\Controls_Manager::URL,
					'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
					'show_external' => true,
					'default' => [
						'url' => '',
						'is_external' => false,
						'nofollow' => false,
					],
				]
			);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'text_typo',
					'selector' => '{{WRAPPER}} .ova_contact_us .text',
				]
			);

			$this->add_control(
				'text_color',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova_contact_us .text a' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'text_hover_color',
				[
					'label' => __( 'Hover Color', 'ova-framework' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova_contact_us .text a:hover' => 'color: {{VALUE}};',
					],
				]
			);

			$this->add_control(
				'icon_spacing',
				[
					'label' => __( 'Spacing', 'plugin-domain' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px', '%' ],
					'range' => [
						'px' => [
							'min' => 5,
							'max' => 200,
						],
						'%' => [
							'min' => 0,
							'max' => 100,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .ova_contact_us .text a' => 'padding-left: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$this->add_control(
				'heading_des',
				[
					'label' => __( 'Color All', 'ova-framework' ),
					'type' => \Elementor\Controls_Manager::HEADING,
					'separator' => 'before',
				]
			);

			$this->add_control(
				'content_color',
				[
					'label' => __( 'Color', 'ova-framework' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova_contact_us .icon, {{WRAPPER}} .ova_contact_us .text a' => 'color: {{VALUE}} !important;',
					],
				]
			);

			$this->add_control(
				'content_hover_color',
				[
					'label' => __( 'Hover Color', 'ova-framework' ),
					'type' => \Elementor\Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova_contact_us:hover .icon, {{WRAPPER}} .ova_contact_us:hover .text a' => 'color: {{VALUE}} !important;',
					],
				]
			);

		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();
		$target = $settings['contact_link']['is_external'] ? ' target="_blank"' : '';
		$nofollow = $settings['contact_link']['nofollow'] ? ' rel="nofollow"' : '';
		$html = '';
		
		$html .= '<div class="ova_contact_us">';
			$html .=	'<i class="icon ' . $settings['icon'] . ' ' . $settings['icon_class'] . '"></i>';
			$html .=	'<div class="text"><a href="'.$settings['contact_link']['url'].'" '. $target . $nofollow .'>'.$settings['text'].'</a></div>';
		$html .= '</div>';
		echo $html;
	}

	
}

