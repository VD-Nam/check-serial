<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use \Elementor\Group_Control_Typography;
use Elementor\Utils;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_header_breadcrumbs extends Widget_Base {

	public function get_name() {
		return 'ova_header_breadcrumbs';
	}

	public function get_title() {
		return __( 'Header Breadcrumbs', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-product-breadcrumbs';
	}

	public function get_categories() {
		return [ 'hf' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);
			
			// Background Color
			$this->add_control(
				'cover_color',
				[
					'label'       => __( 'Background Cover Color', 'ova-framework' ),
					'type'        => Controls_Manager::COLOR,
					'default'     => 'rgba(0,0,0,0.51)',
					'description' => __( 'You can add background image in Advanced Tab', 'ova-framework' ),
					'selectors'   => [
						'{{WRAPPER}} .cover_color' => 'background-color: {{VALUE}};',
					],
					'scheme' => [
						'type'  => \Elementor\Scheme_Color::get_type(),
						'value' => \Elementor\Scheme_Color::COLOR_3,
					],
					'separator' => 'after'
				]
			);

			// Title
			$this->add_control(
				'show_title',
				[
					'label'    => __( 'Show Title', 'ova-framework' ),
					'type'     => Controls_Manager::SWITCHER,
					'default'  => 'yes',
					'selector' => '{{WRAPPER}} .ova_header_el .header_title',
				]
			);
			
			$this->add_control(
				'title_color',
				[
					'label'     => __( 'Title Color', 'ova-framework' ),
					'type'      => Controls_Manager::COLOR,
					'default'   => '#fff',
					'selectors' => [
						'{{WRAPPER}} .ova_header_el .header_title' => 'color: {{VALUE}};',
					],
					'scheme' => [
						'type'  => \Elementor\Scheme_Color::get_type(),
						'value' => \Elementor\Scheme_Color::COLOR_3,
					]
				]
			);

			

			$this->add_group_control(
				\Elementor\Group_Control_Typography::get_type(),
				[
					'name'     => 'header_title',
					'label'    => __( 'Title Typo', 'ova-framework' ),
					'scheme'   => \Elementor\Scheme_Typography::TYPOGRAPHY_3,
					'selector' => '{{WRAPPER}} .ova_header_el .header_title',

				]
			);


			// Breadcrumbs
			$this->add_control(
				'show_breadcrumbs',
				[
					'label'        => __( 'Show Breadcrumbs', 'ova-framework' ),
					'type'         => Controls_Manager::SWITCHER,
					'default'      => 'yes',
					'selector'	=> '{{WRAPPER}} .ovatheme_breadcrumbs_el',
					'separator' => 'before'
				]
			);
			
			$this->add_control(
				'breadcrumbs_color',
				[
					'label'     => __( 'Breadcrumbs Color', 'ova-framework' ),
					'type'      => Controls_Manager::COLOR,
					'default'   => '#fff',
					'selectors' => [
						'{{WRAPPER}} .ova_header_el .ovatheme_breadcrumbs ul.breadcrumb li'         => 'color: {{VALUE}};',
						'{{WRAPPER}} .ova_header_el .ovatheme_breadcrumbs ul.breadcrumb li a'       => 'color: {{VALUE}};',
						'{{WRAPPER}} .ova_header_el .ovatheme_breadcrumbs ul.breadcrumb a'          => 'color: {{VALUE}};',
						'{{WRAPPER}} .ova_header_el .ovatheme_breadcrumbs ul.breadcrumb .separator' => 'color: {{VALUE}};',
					],
					'scheme' => [
						'type'  => \Elementor\Scheme_Color::get_type(),
						'value' => \Elementor\Scheme_Color::COLOR_3,
					]
				]
			);			

			$this->add_group_control(
				\Elementor\Group_Control_Typography::get_type(),
				[
					'name'     => 'header_breadcrumbs_typo',
					'label'    => __( 'Breadcrumbs Typo', 'ova-framework' ),
					'scheme'   => \Elementor\Scheme_Typography::TYPOGRAPHY_3,
					'selector' => '{{WRAPPER}} .ova_header_el .ovatheme_breadcrumbs ul.breadcrumb li',
					
				]
			);

			
			// Style
			$this->add_responsive_control(
				'align',
				[
					'label'   => __( 'Alignment', 'ova-framework' ),
					'type'    => Controls_Manager::CHOOSE,
					'options' => [
						'left' => [
							'title' => __( 'Left', 'ova-framework' ),
							'icon'  => 'fa fa-align-left',
						],
						'center' => [
							'title' => __( 'Center', 'ova-framework' ),
							'icon'  => 'fa fa-align-center',
						],
						'right' => [
							'title' => __( 'Right', 'ova-framework' ),
							'icon'  => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}}' => 'text-align: {{VALUE}};',
					],
					'default'	=> 'center',
					'separator' => 'before'
				]
			);
			

			$this->add_control(
				'class',
				[
					'label' => __( 'Class', 'ova-framework' ),
					'type'  => Controls_Manager::TEXT,
				]
			);

		$this->end_controls_section();

		
	}

	protected function render() {
		$settings = $this->get_settings();

		 ?>
		 	<!-- Display when you choose background per Post -->
		 	<div class="ova_header_el_bg">
		 		<div class="cover_color"></div>
		 	</div>

			<div class="ova_header_el <?php echo esc_attr( $settings['class'] ); ?>">
				
				<?php if( $settings['show_title'] == 'yes' ){ ?>
					<h1 class="second_font header_title">
						<?php echo pogon_framework_the_title(); ?>
					</h1>
				<?php } ?>


				<?php if( function_exists( 'pogon_breadcrumbs_header' ) && $settings['show_breadcrumbs'] == 'yes' ){ ?>
					<div class="ovatheme_breadcrumbs ovatheme_breadcrumbs_el">
						<?php echo pogon_breadcrumbs_header(); ?>
					</div>
				<?php } ?>

			</div>
		<?php
	}

	
}
