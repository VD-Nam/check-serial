<?php

namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Utils;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_testimonial extends Widget_Base {

	public function get_name() {
		return 'ova_testimonial';
	}

	public function get_title() {
		return __( 'Testimonial', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-testimonial';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		// Carousel
		wp_enqueue_style( 'owl-carousel', OVA_PLUGIN_URI.'assets/libs/owl-carousel/assets/owl.carousel.min.css' );
		wp_enqueue_script( 'owl-carousel', OVA_PLUGIN_URI.'assets/libs/owl-carousel/owl.carousel.min.js', array('jquery'), false, true );
		return [ 'script-elementor' ];
	}

	protected function _register_controls() {


		/**********************************************
					CONTENT SECTION
		**********************************************/

		$this->start_controls_section(
			'section_version',
			[
				'label' => __( 'Version', 'ova-framework' ),
			]
		);

		$this->add_control(
			'version',
			[
				'label'   => __( 'Version', 'ova-framework' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'version_1',
				'options' => [
					'version_1' => __('Version 1', 'ova-framework'),
					'version_2' => __('Version 2', 'ova-framework'),
				],
			]
			
		);



		$this->end_controls_section();

		//content version 1
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'condition' => [
					'version' => 'version_1'
				]
			]
		);


			$repeater = new \Elementor\Repeater();

				$repeater->add_control(
					'name_author',
					[
						'label'   => 'Name Author',
						'type'    => \Elementor\Controls_Manager::TEXT,
						'default' => 'Brittany Bailey',
					]
				);

				$repeater->add_control(
					'job',
					[
						'label'   => 'Job',
						'type'    => \Elementor\Controls_Manager::TEXT,
						'default' => __('Customer', 'ova-framework'),
					]
				);

				$repeater->add_control(
					'image_author',
					[
						'label'   => 'Image Author',
						'type'    => \Elementor\Controls_Manager::MEDIA,
						'default' => [
							'url' => Utils::get_placeholder_image_src(),
						],
					]
				);

				$repeater->add_control(
					'testimonial',
					[
						'label'   => __( 'Testimonial ', 'ova-framework' ),
						'type'    => \Elementor\Controls_Manager::TEXTAREA,
						'default' => __( '"Sed ullamcorper morbi tincidunt or massa eget egestas purus. Non nisi est sit amet facilisis magna etiam."', 'ova-framework' ),
					]
				);

				$this->add_control(
					'tab_item',
					[
						'label'       => 'Item Testimonial',
						'type'        => Controls_Manager::REPEATER,
						'fields'      => $repeater->get_controls(),
						'title_field' => '{{{ name_author }}}',
					]
				);
			

		$this->end_controls_section();

		/*****************  END SECTION CONTENT ******************/


		//content version 2

		$this->start_controls_section(
			'section_content_v2',
			[
				'label' => __( 'Content', 'ova-framework' ),
				'condition' => [
					'version' => 'version_2'
				]
			]
		);


			$repeater_2 = new \Elementor\Repeater();

				$repeater_2->add_control(
					'name_author_v2',
					[
						'label'   => 'Name Author',
						'type'    => \Elementor\Controls_Manager::TEXT,
						'default' => 'Peter Roberts',
					]
				);

				$repeater_2->add_control(
					'job_v2',
					[
						'label'   => 'Job',
						'type'    => \Elementor\Controls_Manager::TEXT,
						'default' => __('Develop Team in Warephase', 'ova-framework'),
					]
				);


				$repeater_2->add_control(
					'testimonial_v2',
					[
						'label'   => __( 'Testimonial ', 'ova-framework' ),
						'type'    => \Elementor\Controls_Manager::TEXTAREA,
						'default' => __( '"“We love Pogon and are very appreciative of being associated with your organization.  The experience for us and our customers has been great. Godon customer service ..."', 'ova-framework' ),
					]
				);

				$this->add_control(
					'tab_item_v2',
					[
						'label'       => 'Item Testimonial',
						'type'        => Controls_Manager::REPEATER,
						'fields'      => $repeater_2->get_controls(),
						'title_field' => '{{{ name_author_v2 }}}',
					]
				);
			

		$this->end_controls_section();

		/*****************  END SECTION CONTENT ******************/


		/*****************************************************************
						START SECTION ADDITIONAL
		******************************************************************/

		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'ova-framework' ),
			]
		);


		/***************************  VERSION 1 ***********************/
			$this->add_control(
				'margin_items',
				[
					'label'   => __( 'Margin Right Items', 'ova-framework' ),
					'type'    => Controls_Manager::NUMBER,
					'default' => 30,
				]
				
			);

			$this->add_control(
				'item_number',
				[
					'label'       => __( 'Item Number', 'ova-framework' ),
					'type'        => Controls_Manager::NUMBER,
					'description' => __( 'Number Item', 'ova-framework' ),
					'default'     => 3,
				]
			);

	

			$this->add_control(
				'slides_to_scroll',
				[
					'label'       => __( 'Slides to Scroll', 'ova-framework' ),
					'type'        => Controls_Manager::NUMBER,
					'description' => __( 'Set how many slides are scrolled per swipe.', 'ova-framework' ),
					'default'     => 1,
				]
			);

			$this->add_control(
				'pause_on_hover',
				[
					'label'   => __( 'Pause on Hover', 'ova-framework' ),
					'type'    => Controls_Manager::SWITCHER,
					'default' => 'yes',
					'options' => [
						'yes' => __( 'Yes', 'ova-framework' ),
						'no'  => __( 'No', 'ova-framework' ),
					],
					'frontend_available' => true,
				]
			);


			$this->add_control(
				'infinite',
				[
					'label'   => __( 'Infinite Loop', 'ova-framework' ),
					'type'    => Controls_Manager::SWITCHER,
					'default' => 'yes',
					'options' => [
						'yes' => __( 'Yes', 'ova-framework' ),
						'no'  => __( 'No', 'ova-framework' ),
					],
					'frontend_available' => true,
				]
			);

			$this->add_control(
				'autoplay',
				[
					'label'   => __( 'Autoplay', 'ova-framework' ),
					'type'    => Controls_Manager::SWITCHER,
					'default' => 'yes',
					'options' => [
						'yes' => __( 'Yes', 'ova-framework' ),
						'no'  => __( 'No', 'ova-framework' ),
					],
					'frontend_available' => true,
				]
			);

			$this->add_control(
				'autoplay_speed',
				[
					'label'     => __( 'Autoplay Speed', 'ova-framework' ),
					'type'      => Controls_Manager::NUMBER,
					'default'   => 3000,
					'step'      => 500,
					'condition' => [
						'autoplay' => 'yes',
					],
					'frontend_available' => true,
				]
			);

			$this->add_control(
				'smartspeed',
				[
					'label'   => __( 'Smart Speed', 'ova-framework' ),
					'type'    => Controls_Manager::NUMBER,
					'default' => 500,
				]
			);

			$this->add_control(
				'dot_control',
				[
					'label'   => __( 'Show Dots', 'ova-framework' ),
					'type'    => Controls_Manager::SWITCHER,
					'default' => 'yes',
					'options' => [
						'yes' => __( 'Yes', 'ova-framework' ),
						'no'  => __( 'No', 'ova-framework' ),
					],
					'frontend_available' => true,
				]
			);

		$this->end_controls_section();

		/****************************  END SECTION ADDITIONAL *********************/


		/*************  SECTION content testimonial  *******************/
		$this->start_controls_section(
			'section_content_testimonial',
			[
				'label' => __( 'Content Testimonial', 'ova-framework' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name'     => 'content_testimonial_typography',
					'selector' => '{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info p.evaluate',
					'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				]
			);

			$this->add_control(
				'content_color',
				[
					'label'     => __( 'Color', 'ova-framework' ),
					'type'      => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info p.evaluate' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'content_margin',
				[
					'label'      => __( 'Margin', 'ova-framework' ),
					'type'       => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info p.evaluate' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'content_padding',
				[
					'label'      => __( 'Padding', 'ova-framework' ),
					'type'       => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info p.evaluate' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);


		$this->end_controls_section();
		###############  end section content testimonial  ###############


		/*************  SECTION NAME AUTHOR. *******************/
		$this->start_controls_section(
			'section_author_name',
			[
				'label' => __( 'Author Name', 'ova-framework' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name'     => 'author_name_typography',
					'selector' => '{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info .info span.name',
					'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				]
			);

			$this->add_control(
				'author_name_color',
				[
					'label'     => __( 'Color Author', 'ova-framework' ),
					'type'      => Controls_Manager::COLOR,
					'selectors' => [
						'
						{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info .info span.name' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'author_name_margin',
				[
					'label'      => __( 'Margin', 'ova-framework' ),
					'type'       => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info .info span.name' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'author_name_padding',
				[
					'label'      => __( 'Padding', 'ova-framework' ),
					'type'       => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info .info span.name' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);


		$this->end_controls_section();
		###############  end section author  ###############


		/*************  SECTION NAME JOB. *******************/
		$this->start_controls_section(
			'section_job',
			[
				'label' => __( 'Job', 'ova-framework' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name'     => 'job_typography',
					'selector' => '{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info .info span.job',
					'scheme'   => Scheme_Typography::TYPOGRAPHY_1,
				]
			);

			$this->add_control(
				'job_color',
				[
					'label'     => __( 'Color Job', 'ova-framework' ),
					'type'      => Controls_Manager::COLOR,
					'selectors' => [
						'
						{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info .info span.job' => 'color : {{VALUE}};',
					],
				]
			);

			$this->add_responsive_control(
				'job_margin',
				[
					'label'      => __( 'Margin', 'ova-framework' ),
					'type'       => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info .info span.job' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				'job_padding',
				[
					'label'      => __( 'Padding', 'ova-framework' ),
					'type'       => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'selectors'  => [
						'{{WRAPPER}} .ova-testimonial .slide-testimonials .client_info .info span.job' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);


		$this->end_controls_section();
		###############  end section job  ###############

	}

	protected function render() {
		$settings = $this->get_settings();
		$tab_item = $settings['tab_item'];
		$tab_item_v2 = $settings['tab_item_v2'];

		$version = $settings['version'];

		$data_options['items']              = $settings['item_number'];
		$data_options['slideBy']            = $settings['slides_to_scroll'];
		$data_options['margin']             = $settings['margin_items'];
		$data_options['autoplayHoverPause'] = $settings['pause_on_hover'] === 'yes' ? true : false;
		$data_options['loop']               = $settings['infinite'] === 'yes' ? true : false;
		$data_options['autoplay']           = $settings['autoplay'] === 'yes' ? true : false;
		$data_options['autoplayTimeout']    = $settings['autoplay_speed'];
		$data_options['smartSpeed']         = $settings['smartspeed'];
		$data_options['dots']               = $settings['dot_control'] === 'yes' ? true : false;
		

		?>
		<section class="ova-testimonial">
			<?php if ($version === 'version_1') : ?>
				<div class="slide-testimonials owl-carousel owl-theme <?php echo esc_attr($version) ?>" data-options="<?php echo esc_attr(json_encode($data_options)) ?>">
					<?php if(!empty($tab_item)) : foreach ($tab_item as $item) : ?>
						<div class="item">
							<div class="client_info">
								<?php if( $item['image_author'] != '' ) : ?>
								<img class="client" src="<?php echo esc_attr($item['image_author']['url']) ?>" alt="">
								<?php endif; ?>
								<?php if( $item['testimonial'] != '' ) : ?>
								<p class="evaluate"><?php echo esc_html($item['testimonial']) ?></p>
								<?php endif; ?>
								<div class="info">
									<?php if( $item['name_author'] != '' ) : ?>
									<span class="name"><?php echo esc_html($item['name_author']) ?></span>
									<?php endif; ?>
									<?php if( $item['job'] != '' ) : ?>
									<span class="job"><?php echo esc_html($item['job']) ?></span>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endforeach; endif; ?>
				</div>
			<?php endif ?>

			<?php if ($version === 'version_2') : ?>
				<div class="slide-testimonials owl-carousel owl-theme <?php echo esc_attr($version) ?>" data-options="<?php echo esc_attr(json_encode($data_options)) ?>">
					<?php if(!empty($tab_item_v2)) : foreach ($tab_item_v2 as $item) : ?>
						<div class="item">
							<div class="client_info">
								
								<?php if( $item['testimonial_v2'] != '' ) : ?>
								<p class="evaluate"><?php echo esc_html($item['testimonial_v2']) ?></p>
								<?php endif; ?>
								<div class="info">
									<?php if( $item['name_author_v2'] != '' ) : ?>
									<span class="name"><?php echo esc_html($item['name_author_v2']) ?></span>
									<?php endif; ?>
									<?php if( $item['job_v2'] != '' ) : ?>
									<span class="job"><?php echo esc_html($item['job_v2']) ?></span>
									<?php endif; ?>
								</div>
							</div>
						</div>
					<?php endforeach; endif; ?>
				</div>
			<?php endif ?>
		</section>
		<?php
	}
	// end render
}


