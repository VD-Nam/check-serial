<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Utils;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Control_Media;
use Elementor\Scheme_Color;

if ( ! defined( 'ABSPATH' ) ) exit; 

class video_popup extends Widget_Base {


	public function get_name() {
		return 'video_popup';
	}

	public function get_title() {
		return __( 'Video Popup', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-youtube';
	}


	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		wp_enqueue_script( 'video-popup', OVA_PLUGIN_URI.'assets/libs/video.popup.js', array('jquery'), false, true );
		return [ 'script-elementor' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$this->add_control(
			'version',
			[
				'label' => __('Version', 'ova-framework'),
				'type' => Controls_Manager::SELECT,
				'default' => 'version_1',
				'options' => [
					'version_1' => __('Version 1', 'ova-framework'),
					'version_2' => __('Version 2', 'ova-framework'),
					'version_3' => __('Version 3', 'ova-framework'),
				]
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link Youtube', 'ova-framework' ),
				'type' => Controls_Manager::TEXT,
			]
		);

		$this->add_control(
			'image_bgr',
			[
				'label'   => __( 'Image', 'ova-framework' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$this->add_control(
			'subtitle_vd',
			[
				'label'   => __('Sub Title','ova-framework'),
				'type'    => Controls_Manager::TEXT,
				'default' => __('Watch Video','ova-framework')
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'subtitle_typography',
				'selector' => '{{WRAPPER}} .video-player-work .video-inside-work span',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_subtitle',
			[
				'label' => __( 'Color Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .video-player-work .video-inside-work span' => 'color : {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'padding_subtitle',
			[
				'label' => __( 'Padding', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .video-player-work .video-inside-work span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);


		$this->add_control(
			'title_vd',
			[
				'label'   => __('Title','ova-framework'),
				'type'    => Controls_Manager::TEXT,
				'default' => __('About Us','ova-framework')
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .video-player-work .video-inside-work h3',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_title',
			[
				'label' => __( 'Color Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .video-player-work .video-inside-work h3' => 'color : {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'padding_title',
			[
				'label' => __( 'Padding', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .video-player-work .video-inside-work h3' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_icon_style' );

		$this->start_controls_tab(
			'tab_icon_normal',
			[
				'label' => __( 'Icon Normal', 'ova-framework' ),
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => __( 'Icon Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .video-player-work .video-inside-work #video i' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_background_color',
			[
				'label' => __( 'Background Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_4,
				],
				'selectors' => [
					'{{WRAPPER}} .video-player-work .video-inside-work #video i' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'width_h',
			[
				'label' => __( 'Width, Height Icon', 'ova-framework' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 300,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 99,
				],
				'selectors' => [
					'{{WRAPPER}} .video-player-work .video-inside-work #video i' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_icon_hover',
			[
				'label' => __( 'Icon Hover', 'ova-framework' ),
			]
		);

		$this->add_control(
			'icon_hover_color',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .video-player-work .video-inside-work #video i:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_background_hover_color',
			[
				'label' => __( 'Background Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .video-player-work .video-inside-work #video i:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'bgr_overlay',
			[
				'label'   => __('Background Overlay','ova-framework'),
				'type'    => Controls_Manager::COLOR,
				'default' => '#efbbab',
				'selectors' => [
					'{{WRAPPER}} .video-player-work:after' => 'background-color: {{VALUE}};'
				]
			]
		);

		$this->add_responsive_control(
			'padding_content',
			[
				'label' => __( 'Padding Content', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .video-player-work' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);


		$this->end_controls_section();

		
	}

	protected function render() {
		$settings = $this->get_settings();

		?>

		<div class="video-player-work <?php echo esc_attr($settings['version']) ?>" style="background: url(<?php echo esc_attr($settings['image_bgr']['url']);?>);">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="video-inside-work">
							<a id="video" class="btn" video-url="<?php echo esc_attr($settings['link']);?>" style="cursor: pointer;"><i class="fas fa-play fa-2x"></i></a>
							<?php if( $settings['subtitle_vd'] != '' ) : ?>
								<div class="content">
									<span><?php echo esc_html($settings['subtitle_vd']);?></span>
								<?php endif;?>
								<?php if( $settings['title_vd'] != '' ) : ?>
									<h3><?php echo esc_html($settings['title_vd']);?></h3>
								<?php endif;?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	<?php }

}
