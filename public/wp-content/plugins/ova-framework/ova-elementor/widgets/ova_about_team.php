<?php

namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_about_team extends Widget_Base {

	public function get_name() {
		return 'ova_about_team';
	}

	public function get_title() {
		return __( 'About Team', 'ova-framework' );
	}

	public function get_icon() {
		return 'eicon-person';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function _register_controls() {


		/**********************************************
		CONTENT SECTION
		**********************************************/
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);

		$this->add_control(
			'version',
			[
				'label' => __( 'Version', 'ova-framework' ),
				'type' => Controls_Manager::SELECT,
				'default' => __('version_1', 'ova-framework'),
				'options' => [
					'version_1' => __('Version 1', 'ova-framework'),
					'version_2' => __('Version 2', 'ova-framework'),
				],
			]
		);


		$this->add_control(
			'name',
			[
				'label' => __( 'Name', 'ova-framework' ),
				'type' => Controls_Manager::TEXT,
				'default' => __('Sharon Welch', 'ova-framework'),
			]
		);

		$this->add_control(
			'job',
			[
				'label' => __( 'Job', 'ova-framework' ),
				'type' => Controls_Manager::TEXTAREA,
				'row' => 2,
				'default' => __("Founder Pogon", "ova-framework"),
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Image', 'ova-framework' ),
				'type' => Controls_Manager::MEDIA,
			]
		);

		$this->add_control(
			'link_about',
			[
				'label' => __( 'Link', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'ova-framework' ),
				'default' => [
					'url' => '#',
					'is_external' => false,
					'nofollow' => false,
				],
			]
		);


		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'icon',
			[
				'label' => __( 'Description Strong', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 2,
			]
		);

		$repeater->add_control(
			'link',
			[
				'label' => __( 'Link', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'ova-framework' ),
				'default' => [
					'url' => '#',
					'is_external' => false,
					'nofollow' => false,
				],
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => __( 'Tabs', 'ova-framework' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'icon' => __('fa fa-facebook', 'ova-framework'),
					],
					[
						'icon' => __('fa fa-twitter', 'ova-framework'),
					],
					[
						'icon' => __('fa fa-tumblr', 'ova-framework'),
					],
					[
						'icon' => __('fa fa-pinterest-p', 'ova-framework'),
					],
					
				],
				'title_field' => '{{{ icon }}}',
			]
		);


		$this->end_controls_section();


		//section style title
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .ova-heading h3.heading-title',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_title',
			[
				'label' => __( 'Color Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-heading h3.heading-title' => 'color : {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'margin_title',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-heading h3.heading-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		//section style sub title
		$this->start_controls_section(
			'section_sub_title',
			[
				'label' => __( 'Sub Title', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'sub_title_typography',
				'selector' => '{{WRAPPER}} .ova-heading .sub-title p',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_sub_title',
			[
				'label' => __( 'Color Sub Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-heading .sub-title' => 'color : {{VALUE}};',
					'{{WRAPPER}} .ova-heading .sub-title p' => 'color : {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'margin_sub_title',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-heading .sub-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		//section style image
		$this->start_controls_section(
			'section_image',
			[
				'label' => __( 'Image', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'margin_image',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-heading .image' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();


	}

	protected function render() {
		$settings = $this->get_settings();

		$name = $settings['name'];
		$job = $settings['job'];
		$tabs = $settings['tabs'];

		?>
		<div class="ova-about-team <?php echo esc_attr($settings['version']) ?>">
			<div class="ova-media">
				<img src="<?php echo esc_attr($settings['image']['url']) ?>" alt="<?php echo esc_attr($name) ?>">
				<div class="list-con">
					<ul>
						<?php if (!empty($tabs)) : foreach($tabs as $item) : ?>
							<li><a target="_blank" href="<?php echo esc_attr($item['link']['url']) ?>"><i class="<?php echo esc_attr($item['icon']) ?>"></i></a></li>
						<?php endforeach; endif; ?>
					</ul>
				</div>
			</div>
			<div class="ova-content">
				<?php if ($name !== "") : ?>
					<h3 class="name"><a href="<?php echo esc_attr($settings['link_about']['url']) ?>"><?php echo esc_html($name) ?></a></h3>
				<?php endif ?>
				<?php if ($job !== "") : ?>
					<p class="job"><?php echo esc_html($job) ?></p>
				<?php endif ?>
			</div>
		</div>
		<?php
	}
// end render
}


