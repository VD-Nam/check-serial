<?php

namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_contact extends Widget_Base {

	public function get_name() {
		return 'ova_contact';
	}

	public function get_title() {
		return __( 'Contact', 'ova-framework' );
	}

	public function get_icon() {
		return 'fa fa-map-marker';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function _register_controls() {


		/**********************************************
		CONTENT SECTION
		**********************************************/

		$this->start_controls_section(
			'section_version_contact',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);
		$this->add_control(
			'version',
			[
				'label' => __( 'version', 'ova-framework' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'version_1',
				'options' => [
					'version_1' => __('Version 1', 'ova-framework'),
					'version_2' => __('Version 2', 'ova-framework'),
				]
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_heading_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
				
			]
		);

		$this->add_control(
			'type_link',
			[
				'label' => __( 'Type Link', 'ova-framework' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'orther',
				'options' => [
					'email' => __('Email', 'ova-framework'),
					'tell' => __('Tell', 'ova-framework'),
					'none' => __('None', 'ova-framework'),
				]
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'ova-framework' ),
				'show_external' => false,
				'condition' => [
					'type_link' => 'orther',
				],
				'default' => [
					'url' => '#',
					'is_external' => false,
					'nofollow' => false,
				],
			]
		);

		$this->add_control(
			'email',
			[
				'label' => __( 'Email', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'condition' => [
					'type_link' => 'email',
				],
				'default' => __('example@email.com', 'ova-framework'),
			]
		);

		$this->add_control(
			'phone',
			[
				'label' => __( 'Phone', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'condition' => [
					'type_link' => 'tell',
				],
				'default' => __('+1(131)839-6275', 'ova-framework'),
			]
		);


		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'ova-framework' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'ova-framework' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'ova-framework' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'center',
				'selectors' => [
					'{{WRAPPER}} .ova-contact' => 'text-align: {{VALUE}}',
				]
			]
		);

		$this->add_control(
			'icon_1',
			[
				'label' => __( 'Icon', 'ova-framework' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'flaticon-map',
				'condition' => [
					'version'=>'version_1',
				]
			]
		);

		$this->add_control(
			'icon_2',
			[
				'label' => __( 'Icon', 'ova-framework' ),
				'type' => Controls_Manager::TEXT,
				'default' => 'flaticon-facebook-placeholder-for-locate-places-on-maps',
				'condition' => [
					'version'=>'version_2',
				]
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'type' => Controls_Manager::TEXT,
				'default' =>__('Address Location', 'ova-framework'),
				'condition' => [
					'version'=>'version_1',
				]
			]
		);

		$this->add_control(
			'sub_title',
			[
				'label' => __( 'Sub Title', 'ova-framework' ),
				'type' => Controls_Manager::TEXTAREA,
				'row' => 2,
				'default' => __("405 Broadway, New York, NY 10013", "ova-framework"),
			]
		);

		$this->end_controls_section();


		//section style title
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .ova-contact .title',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_title',
			[
				'label' => __( 'Color Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-contact .title' => 'color : {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'margin_title',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-contact .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		//section style sub title
		$this->start_controls_section(
			'section_sub_title',
			[
				'label' => __( 'Sub Title', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'sub_title_typography',
				'selector' => '{{WRAPPER}} .ova-contact .sub-title',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_sub_title',
			[
				'label' => __( 'Color Sub Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-contact .sub-title' => 'color : {{VALUE}};'
				],
			]
		);

		$this->add_responsive_control(
			'margin_sub_title',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-contact .sub-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		//section style image
		$this->start_controls_section(
			'section_icon',
			[
				'label' => __( 'Icon', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'fontsize_icon',
			[
				'label' => __( 'Font Size', 'ova-framework' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 200,
						'step' => 1,
					]
				],
				'selectors' => [
					'{{WRAPPER}} .ova-contact .icon i:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'color_icon',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-contact .icon i:before' => 'color : {{VALUE}};'
				],
			]
		);

		$this->add_responsive_control(
			'margin_icon',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-contact .icon' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();


	}

	protected function render() {
		$settings = $this->get_settings();
		$type_link = $settings['type_link'];
		$link = $settings['link']['url'];
		$email = $settings['email'];
		$phone = $settings['phone'];
		$href = "";
		switch($type_link) {
			case "email" : {
				$href = "href='mailto:".esc_attr($email)."'";
				break;
			}
			case "tell" : {
				$href = "href='tel:".esc_attr($phone)."'";
				break;
			}
		}

		$icon = "";
		$version = $settings['version'];
		if ($version === 'version_1') {
			$icon = $settings['icon_1'] ;
		} else if ($version === 'version_2') {
			$icon = $settings['icon_2'] ;
		}
		
		$title = $settings['title'];
		$sub_title = $settings['sub_title'];
		?>
		<div class="ova-contact <?php echo esc_attr($version) ?>">
			<?php if (!empty($icon)) : ?>
				<div class="icon">
					<i class="<?php echo esc_attr($icon) ?>"></i>
				</div>
			<?php endif ?>
			<?php if (!empty($title)) : ?>
				<h3 class="title"><?php echo esc_html($title) ?></h3>
			<?php endif ?>
			<?php if (!empty($sub_title)) : ?>
				<?php if ($href !== "") { ?>
					<a <?php echo $href ?> class="sub-title"><?php echo esc_html($sub_title) ?></a>
				<?php } else { ?>
					<p <?php echo $href ?> class="sub-title"><?php echo esc_html($sub_title) ?></p>
				<?php } ?>
			<?php endif ?>
		</div>
		<?php

	}
// end render
}


