<?php

namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_price_tab extends Widget_Base {

	public function get_name() {
		return 'ova_price_tab';
	}

	public function get_title() {
		return __( 'Price Tab', 'ova-framework' );
	}

	public function get_icon() {
		return 'fa fa-table';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function _register_controls() {


		/**********************************************
		CONTENT SECTION
		**********************************************/
		$this->start_controls_section(
			'section_heading_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);


		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'ova-framework' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'ova-framework' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'ova-framework' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'left',
				'selectors' => [
					'{{WRAPPER}} .ova-tab-price' => 'text-align: {{VALUE}}',
				]
			]
		);



		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'text_button',
			[
				'label' => __( 'Text Button', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Month', 'ova-framework'),
			]
		);

		$repeater->add_control(
			'id_button',
			[
				'label' => __( 'Id Button', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'rows' => 2,
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => __( 'Tabs', 'ova-framework' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'text_button' => __('Month', 'ova-framework'),
						'id_button' => __('tab-month', 'ova-framework'),
					],
					[
						'text_button' => __('Years', 'ova-framework'),
						'id_button' => __('tab-years', 'ova-framework'),
					],
				],
				'title_field' => '{{{ text_button }}}',
			]
		);

		$this->end_controls_section();


	}

	protected function render() {
		$settings = $this->get_settings();
		$tabs = $settings['tabs'];
		?>
		<div class="ova-tab-price">
			<div class="tab-price">
				<?php if (!empty($tabs)) : foreach ($tabs as $item) : ?>
				<button id="<?php echo esc_attr($item['id_button']) ?>"><?php echo esc_html($item['text_button']) ?></button>
				<?php endforeach; endif ?>
			</div>
		</div>
		<?php

	}
// end render
}


