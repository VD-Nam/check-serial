<?php
namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


class ova_price_table extends Widget_Base {

	public function get_name() {
		return 'ova_price_table';
	}

	public function get_title() {
		return __( 'Price Table', 'ova-framework' );
	}

	public function get_icon() {
		return 'fa fa-table';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_version',
			[
				'label' => __( 'Price Table', 'ova-framework' ),
			]
		);

		$this->add_control(
			'version_price_table',
			[
				'label' => __( 'Version', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'version_1',
				'options' => [
					'version_1' => __("Version 1", "ova-framework"),
					'version_2' => __("Version 2", "ova-framework"),
				]
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Price Table', 'ova-framework' ),
				'condition' => [
					'version_price_table' => 'version_1'
				]
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Basic Plan', 'ova-framework'),
			]
		);

		$this->add_control(
			'show_recommended',
			[
				'label' => __( 'Show Recommended', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'ova-framework' ),
				'label_off' => __( 'Hide', 'ova-framework' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
		);

		$this->add_control(
			'text_recommended',
			[
				'label' => __( 'Text Recommended', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Recommended', 'ova-framework'),
				'condition' => [
					'show_recommended' => 'yes',
				]
			]
		);

		$this->add_control(
			'price',
			[
				'label' => __( 'Price', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('10', 'ova-framework'),
			]
		);

		$this->add_control(
			'unit',
			[
				'label' => __( 'Unit', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('$', 'ova-framework'),
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'des_strong',
			[
				'label' => __( 'Description Strong', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 2,
			]
		);

		$repeater->add_control(
			'desc',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 2,
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => __( 'Tabs', 'ova-framework' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'des_strong' => __('2GB', 'ova-framework'),
						'desc' => __('Bandwidth', 'ova-framework'),
					],
					[
						'des_strong' => __('100GB', 'ova-framework'),
						'desc' => __('Storage', 'ova-framework'),
					],
					[
						'des_strong' => __('2', 'ova-framework'),
						'desc' => __('Accounts', 'ova-framework'),
					],
					[
						'des_strong' => __('1', 'ova-framework'),
						'desc' => __('Host Domain', 'ova-framework'),
					],
					[
						'des_strong' => __('24/7', 'ova-framework'),
						'desc' => __('Support', 'ova-framework'),
					],
				],
				'title_field' => '{{{ desc }}}',
			]
		);



		$this->add_control(
			'text_button',
			[
				'label' => __( 'Text Button', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Purchase Now', 'ova-framework'),
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'ova-framework' ),
				'default' => [
					'url' => '#',
					'is_external' => false,
					'nofollow' => false,
				],
			]
		);


		$this->end_controls_section();



		//start section version 2
		$this->start_controls_section(
			'section_content_v2',
			[
				'label' => __( 'Price Table', 'ova-framework' ),
				'condition' => [
					'version_price_table' => 'version_2'
				]
			]
		);

		$this->add_control(
			'title_v2',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Agency Plan', 'ova-framework'),
			]
		);

		$this->add_control(
			'image_icon_v2',
			[
				'label' => __( 'Image Icon', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
			]
		);

		

		$repeater_v2 = new \Elementor\Repeater();

		$repeater_v2->add_control(
			'des_strong_v2',
			[
				'label' => __( 'Description Strong', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 2,
			]
		);

		$repeater_v2->add_control(
			'desc_v2',
			[
				'label' => __( 'Description', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 2,
			]
		);

		$this->add_control(
			'tabs_v2',
			[
				'label' => __( 'Tabs', 'ova-framework' ),
				'type' => Controls_Manager::REPEATER,
				'fields' => $repeater_v2->get_controls(),
				'default' => [
					[
						'des_strong_v2' => __('2GB', 'ova-framework'),
						'desc_v2' => __('Bandwidth', 'ova-framework'),
					],
					[
						'des_strong_v2' => __('100GB', 'ova-framework'),
						'desc_v2' => __('Storage', 'ova-framework'),
					],
					[
						'des_strong_v2' => __('02', 'ova-framework'),
						'desc_v2' => __('Accounts', 'ova-framework'),
					],
					[
						'des_strong_v2' => __('24/7', 'ova-framework'),
						'desc_v2' => __('Support', 'ova-framework'),
					],
					[
						'des_strong_v2' => __('Custom Email', 'ova-framework'),
						'desc_v2' => __('Support', 'ova-framework'),
					],
				],
				'title_field' => '{{{ desc_v2 }}}',
			]
		);
		$this->add_control(
			'price_v2',
			[
				'label' => __( 'Price', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('10', 'ova-framework'),
			]
		);

		$this->add_control(
			'unit_v2',
			[
				'label' => __( 'Unit', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('$', 'ova-framework'),
			]
		);

		$this->add_control(
			'pertime_v2',
			[
				'label' => __( 'Pertime', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('month', 'ova-framework'),
			]
		);


		$this->add_control(
			'text_button_v2',
			[
				'label' => __( 'Text Button', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __('Purchase Now', 'ova-framework'),
			]
		);

		$this->add_control(
			'link_v2',
			[
				'label' => __( 'Link', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'ova-framework' ),
				'default' => [
					'url' => '#',
					'is_external' => false,
					'nofollow' => false,
				],
			]
		);


		$this->end_controls_section();
		//end section content version 2

		// end tab section_content

		/* Style Primary color */
		$this->start_controls_section(
			'section_style_primary_color',
			[
				'label' => __( 'Primary Color', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_1'
				]
			]
		);

		$this->add_control(
			'primary_color',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table .recommended button ' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .ova-price-table .wp-price .price:after' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .ova-price-table .button-price a' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .ova-price-table .button-price a:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'primary_color_hover',
			[
				'label' => __( 'Color Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table .button-price a:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();


		/* Style title */
		$this->start_controls_section(
			'section_style_title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_1'
				]
			]
		);

		$this->add_responsive_control(
			'title_margin',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-price-table .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table .title h3 ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .ova-price-table .title h3 ',
			]
		);
		$this->end_controls_section();

		/* Style Recommended */
		$this->start_controls_section(
			'section_style_recommended',
			[
				'label' => __( 'Recommended', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_1'
				]
			]
		);


		$this->add_control(
			'recommen_color',
			[
				'label' => __( 'Recommended', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table .recommended button' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'recommen_typography',
				'selector' => '{{WRAPPER}} .ova-price-table .recommended button ',
			]
		);
		$this->end_controls_section();


		/* Style Price */
		$this->start_controls_section(
			'section_style_price',
			[
				'label' => __( 'Price Color', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_1'
				]
			]
		);

		$this->add_control(
			'price_color',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table .wp-price .price ' => 'color: {{VALUE}}',
					'{{WRAPPER}} .ova-price-table .wp-price .unit ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();


		/* Style desc strong */
		$this->start_controls_section(
			'section_style_desc_strong',
			[
				'label' => __( 'Description Strong ', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_1'
				]
			]
		);


		$this->add_control(
			'desc_strong_color',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table .desc ul li span.strong ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'desc_strong_typography',
				'selector' => '{{WRAPPER}} .ova-price-table .desc ul li span.strong ',
			]
		);
		$this->end_controls_section();

		/* Style desc */
		$this->start_controls_section(
			'section_style_desc',
			[
				'label' => __( 'Description ', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_1'
				]
			]
		);


		$this->add_control(
			'desc_color',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table .desc ul li span:not(.strong) ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'desc_typography',
				'selector' => '{{WRAPPER}} .ova-price-table .desc ul li span:not(.strong)',
			]
		);
		$this->end_controls_section();




		/*************************************************************
					TAB STYLE VERSION 2
		**************************************************************/


		/* Style Primary color */
		$this->start_controls_section(
			'section_style_primary_color_v2',
			[
				'label' => __( 'Primary Color', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_2'
				]
			]
		);

		$this->add_control(
			'primary_color_v2',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table.version_2 .title' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .ova-price-table.version_2 .wp-price' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .ova-price-table.version_2 .button-price a:hover' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();


		/* Style title */
		$this->start_controls_section(
			'section_style_title_v2',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_2'
				]
			]
		);

		$this->add_responsive_control(
			'title_margin_v2',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-price-table.version_2 .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'title_color_v2',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table.version_2 .title h3 ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_v2_typography',
				'selector' => '{{WRAPPER}} .ova-price-table.version_2 .title h3 ',
			]
		);
		$this->end_controls_section();


		/* Style Price */
		$this->start_controls_section(
			'section_style_price_v2',
			[
				'label' => __( 'Price Color', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_2'
				]
			]
		);

		$this->add_control(
			'price_color_v2',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table.version_2 .wp-price span ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();


		/* Style desc strong */
		$this->start_controls_section(
			'section_style_desc_strong_v2',
			[
				'label' => __( 'Description Strong ', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_2'
				]
			]
		);


		$this->add_control(
			'desc_strong_color_v2',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table.version_2 .desc ul li span.strong ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'desc_v2_strong_typography',
				'selector' => '{{WRAPPER}} .ova-price-table.version_2 .desc ul li span.strong ',
			]
		);
		$this->end_controls_section();

		/* Style desc */
		$this->start_controls_section(
			'section_style_desc_v2',
			[
				'label' => __( 'Description ', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'version_price_table' => 'version_2'
				]
			]
		);


		$this->add_control(
			'desc_color_v2',
			[
				'label' => __( 'Color', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-price-table.version_2 .desc ul li span:not(.strong) ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'desc_v2_typography',
				'selector' => '{{WRAPPER}} .ova-price-table.version_2 .desc ul li span:not(.strong)',
			]
		);
		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings_for_display();
		$tabs = $settings['tabs'];
		$tabs_v2 = $settings['tabs_v2'];
		$class_recommended = ($settings['show_recommended'] === 'yes') ? ' recommended ' : '';
		
		$version = $settings['version_price_table'];

		?>

		<div class="ova-price-table <?php echo esc_attr($class_recommended) ?> <?php echo esc_attr($version) ?>">
			<?php if ($version === 'version_1') : ?>
				<?php if ($settings['show_recommended'] === 'yes') : ?>
					<div class="recommended">
						<button><?php echo esc_html($settings['text_recommended']) ?></button>
					</div>
				<?php endif ?>
				<div class="title">
					<h3><?php echo esc_html($settings['title']) ?></h3>
				</div>
				<div class="wp-price">
					<span class="unit"><?php echo esc_html($settings['unit']) ?></span>
					<span class="price"><?php echo esc_html($settings['price']) ?></span>
				</div>
				<div class="desc">
					<ul>
						<?php if (!empty($tabs)) : foreach ($tabs as $item) : ?>
							<li><span class="strong"><?php echo esc_html($item['des_strong']) ?></span><span><?php echo esc_html($item['desc']) ?></span></li>
						<?php endforeach; endif ?>
					</ul>
				</div>
				<div class="button-price">
					<a href="<?php echo esc_attr($settings['link']['url']) ?>"><?php echo esc_html($settings['text_button']) ?></a>			
				</div>
			<?php endif ?>

			<?php if ($version === 'version_2') : ?>
				<div class="title">
					<h3><?php echo esc_html($settings['title_v2']) ?></h3>
				</div>
				<div class="image" >
					<div class="img">
						<img src="<?php echo esc_attr($settings['image_icon_v2']['url']) ?>">
					</div>
				</div>
				
				<div class="desc">
					<ul>
						<?php if (!empty($tabs_v2)) : foreach ($tabs_v2 as $item) : ?>
							<li><span class="strong"><?php echo esc_html($item['des_strong_v2']) ?></span><span><?php echo esc_html($item['desc_v2']) ?></span></li>
						<?php endforeach; endif ?>
					</ul>
				</div>
				<div class="wp-price">
					<div class="top">
						<span class="unit"><?php echo esc_html($settings['unit_v2']) ?></span>
						<span class="price"><?php echo esc_html($settings['price_v2']) ?></span>
					</div>
					<span class="pertime"><?php echo esc_html($settings['pertime_v2']) ?></span>
				</div>
				<div class="button-price">
					<a href="<?php echo esc_attr($settings['link_v2']['url']) ?>"><?php echo esc_html($settings['text_button_v2']) ?></a>			
				</div>
			<?php endif ?>
		</div>
		<?php
	}
}
