<?php

namespace ova_framework\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class ova_service extends Widget_Base {

	public function get_name() {
		return 'ova_service';
	}

	public function get_title() {
		return __( 'Service', 'ova-framework' );
	}

	public function get_icon() {
		return 'fa fa-address-card';
	}

	public function get_categories() {
		return [ 'ovatheme' ];
	}

	public function get_script_depends() {
		return [ 'script-elementor' ];
	}

	protected function _register_controls() {


		/**********************************************
		CONTENT SECTION
		**********************************************/
		$this->start_controls_section(
			'section_heading_content',
			[
				'label' => __( 'Content', 'ova-framework' ),
			]
		);



		$this->add_control(
			'type',
			[
				'label' => __( 'Type', 'ova-framework' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'one_column',
				'options' => [
					'one_column' => __("One Column", 'ova-framework'),
					'one_column_box' => __("One Column Box", 'ova-framework'),
					'two_column' => __("Two Column", 'ova-framework'),
					'version_4' => __("Version 4", 'ova-framework'),
				]
			]
		);

		$this->add_control(
			'image',
			[
				'label' => __( 'Image', 'ova-framework' ),
				'type' => Controls_Manager::MEDIA,
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'type' => Controls_Manager::TEXT,
				'default' => __('Expert Advice', 'ova-framework'),
			]
		);

		$this->add_control(
			'sub_title',
			[
				'label' => __( 'Sub Title', 'ova-framework' ),
				'type' => Controls_Manager::TEXTAREA,
				'row' => 5,
				'default' => __("Facilisis magna etiam tempor orci eu. Amet consectetur cursus mattis molestie a iaculis at erat.", "ova-framework"),
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link', 'ova-framework' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'ova-framework' ),
				'default' => [
					'url' => '#',
					'is_external' => false,
					'nofollow' => false,
				],
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'border',
				'label' => __( 'Border', 'ova-framework' ),
				'selector' => '{{WRAPPER}} .ova-service',
				'condition' => [
					'type' => 'two_column',
				]
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Border::get_type(),
			[
				'name' => 'border_one_box',
				'label' => __( 'Border', 'ova-framework' ),
				'selector' => '{{WRAPPER}} .ova-service',
				'condition' => [
					'type' => 'one_column_box',
				]
			]
		);


		$this->end_controls_section();

		//section style padding box
		$this->start_controls_section(
			'section_padding_box',
			[
				'label' => __( 'Box', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'padding_box',
			[
				'label' => __( 'Padding Box', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-service' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		$this->add_responsive_control(
			'margin_box',
			[
				'label' => __( 'Margin Box', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-service' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		$this->add_control(
			'color_box_hover',
			[
				'label' => __( 'Color Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-service.one_column_box:hover ' => 'background-color : {{VALUE}};border-color : {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();


		//section style title
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .ova-service .content .title a',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_title',
			[
				'label' => __( 'Color Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-service .content .title a' => 'color : {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'color_title_hover',
			[
				'label' => __( 'Color Title Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-service .content .title a:hover' => 'color : {{VALUE}} !important;',
				],
			]
		);

		$this->add_control(
			'color_title_box_hover',
			[
				'label' => __( 'Color Title Box Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-service:hover .content .title a' => 'color : {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'margin_title',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-service .content .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		//section style sub title
		$this->start_controls_section(
			'section_sub_title',
			[
				'label' => __( 'Sub Title', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'sub_title_typography',
				'selector' => '{{WRAPPER}} .ova-service .content .sub-title',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$this->add_control(
			'color_sub_title',
			[
				'label' => __( 'Color Sub Title', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-service .content .sub-title' => 'color : {{VALUE}} ;',
				],
			]
		);

		$this->add_control(
			'color_sub_title_box_hover',
			[
				'label' => __( 'Color Sub Title Box Hover', 'ova-framework' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .ova-service:hover .content .sub-title' => 'color : {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'margin_sub_title',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-service .content .sub-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		//section style image
		$this->start_controls_section(
			'section_image',
			[
				'label' => __( 'Image', 'ova-framework' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'margin_image',
			[
				'label' => __( 'Margin', 'ova-framework' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .ova-service .image img' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
				],
			]
		);

		$this->end_controls_section();


	}

	protected function render() {
		$settings = $this->get_settings();

		$title = $settings['title'];
		$sub_title = $settings['sub_title'];
		$img_heading = $settings['image']['url'];
		?>
		<div class="ova-service <?php echo esc_attr($settings['type']) ?>">
			<?php if (!empty($img_heading)) : ?>
				<div class="image">
					<div class="img">
						<img src="<?php echo $img_heading ?>" alt="<?php echo esc_attr($title) ?>">
					</div>
				</div>
			<?php endif ?>
			<div class="content">
				<?php if (!empty($title)) : ?>
					<h3 class="title"><a href="<?php echo esc_attr($settings['link']['url']) ?>"><?php echo esc_html($title) ?></a></h3>
				<?php endif ?>
				<?php if (!empty($sub_title)) : ?>
					<p class="sub-title">
						<?php echo $sub_title ?>
					</p>
				<?php endif ?>
			</div>
		</div>
		<?php

	}
// end render
}


