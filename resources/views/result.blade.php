
<!DOCTYPE html>
<html lang="vi" >

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="profile" href="//gmpg.org/xfn/11">
    <link rel="pingback" href="https://honganh.vn/xmlrpc.php">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>

    <!-- This site is optimized with the Yoast SEO plugin v15.5 - https://yoast.com/wordpress/plugins/seo/ -->
    <title>Bảo hành</title>
    <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <meta property="og:locale" content="vi_VN" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Bảo hành - Công ty TNHH Máy tính Hồng Anh" />
    <meta property="og:site_name" content="Công ty TNHH Máy tính Hồng Anh" />
    <meta property="article:modified_time" content="2021-01-14T02:10:15+00:00" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:label1" content="Est. reading time">
    <meta name="twitter:data1" content="0 phút">
    <script type="application/ld+json" class="yoast-schema-graph">{"@context":"https://schema.org","@graph":[{"@type":"WebSite","@id":"https://honganh.vn/#website","url":"https://honganh.vn/","name":"C\u00f4ng ty TNHH M\u00e1y t\u00ednh H\u1ed3ng Anh","description":"Chuy\u00ean cung c\u1ea5p b\u00e1n bu\u00f4n linh ph\u1ee5 ki\u1ec7n desktop, laptop, thi\u1ebft b\u1ecb m\u00e1y t\u00ednh,","potentialAction":[{"@type":"SearchAction","target":"https://honganh.vn/?s={search_term_string}","query-input":"required name=search_term_string"}],"inLanguage":"vi"},{"@type":"WebPage","@id":"https://honganh.vn/test-san-pham/#webpage","url":"https://honganh.vn/test-san-pham/","name":"Test San Pham - C\u00f4ng ty TNHH M\u00e1y t\u00ednh H\u1ed3ng Anh","isPartOf":{"@id":"https://honganh.vn/#website"},"datePublished":"2021-01-13T08:39:21+00:00","dateModified":"2021-01-14T02:10:15+00:00","inLanguage":"vi","potentialAction":[{"@type":"ReadAction","target":["https://honganh.vn/test-san-pham/"]}]}]}</script>
    <!-- / Yoast SEO plugin. -->


    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Dòng thông tin Công ty TNHH Máy tính Hồng Anh &raquo;" href="https://honganh.vn/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Dòng phản hồi Công ty TNHH Máy tính Hồng Anh &raquo;" href="https://honganh.vn/comments/feed/" />
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.0.1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/honganh.vn\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.6"}};
        !function(e,a,t){var r,n,o,i,p=a.createElement("canvas"),s=p.getContext&&p.getContext("2d");function c(e,t){var a=String.fromCharCode;s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,e),0,0);var r=p.toDataURL();return s.clearRect(0,0,p.width,p.height),s.fillText(a.apply(this,t),0,0),r===p.toDataURL()}function l(e){if(!s||!s.fillText)return!1;switch(s.textBaseline="top",s.font="600 32px Arial",e){case"flag":return!c([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])&&(!c([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!c([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]));case"emoji":return!c([55357,56424,8205,55356,57212],[55357,56424,8203,55356,57212])}return!1}function d(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(i=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},o=0;o<i.length;o++)t.supports[i[o]]=l(i[o]),t.supports.everything=t.supports.everything&&t.supports[i[o]],"flag"!==i[o]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[i[o]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(r=t.source||{}).concatemoji?d(r.concatemoji):r.wpemoji&&r.twemoji&&(d(r.twemoji),d(r.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='wp-block-library-css'  href={{ asset('wp-includes/css/dist/block-library/style.min.css?ver=5.6') }} type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-vendors-style-css'  href={{ asset('wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/vendors-style.css?ver=3.8.1') }} type='text/css' media='all' />
    <link rel='stylesheet' id='wc-block-style-css'  href={{ asset('wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=3.8.1') }} type='text/css' media='all' />
    <link rel='stylesheet' id='contact-form-7-css'  href={{ asset('wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.3.2') }} type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required { visibility: visible; }
    </style>
    <link rel='stylesheet' id='owl.carousel.style-css'  href={{ asset('wp-content/plugins/wp-posts-carousel/owl.carousel/assets/owl.carousel.css?ver=5.6') }} type='text/css' media='all' />
    <link rel='stylesheet' id='hpr-style-css'  href={{ asset('wp-content/plugins/hotline-phone-ring/assets/css/style-2.css?ver=2.0.5') }} type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap-css'  href={{ asset('wp-content/themes/pogon/assets/libs/bootstrap/css/bootstrap.min.css') }} type='text/css' media='all' />
    <link rel='stylesheet' id='v4-shims-css'  href={{ asset('wp-content/themes/pogon/assets/libs/fontawesome/css/v4-shims.min.css') }} type='text/css' media='all' />

    <link rel='stylesheet' id='fontawesome-css'  href={{ asset('wp-content/themes/pogon/assets/libs/fontawesome/css/all.min.css') }} type='text/css' media='all' />
    {{--    <link rel='stylesheet' id='elegant-font-css'  href="{{ asset('css/ele_style.css') }}" type='text/css' media='all' />--}}
    <link rel="stylesheet" id="elegant-font-css" href={{ asset("wp-content/themes/pogon/assets/libs/elegant_font/ele_style.css") }} type="text/css" media="all">
    <link rel='stylesheet' id='themify-icon-css'  href="{{ asset('css/themify-icons.css') }}" type='text/css' media='all' />
    <link rel='stylesheet' id='flaticon-css'  href={{ asset('wp-content/themes/pogon/assets/libs/flaticon/font/flaticon.css') }} type='text/css' media='all' />
    <link rel='stylesheet' id='pogon-theme-css'  href={{ asset('wp-content/themes/pogon/assets/css/theme.css') }} type='text/css' media='all' />
    <link rel='stylesheet' id='pogon-style-css'  href={{ asset('wp-content/themes/pogon/style.css') }} type='text/css' media='all' />
    <style id='pogon-style-inline-css' type='text/css'>

        body{
            font-family: Open Sans;
            font-weight: 400;
            font-size: 15px;
            line-height: 1.8em;
            letter-spacing: 0px;
            color: #666;
        }
        p{
            color: #666;
            line-height: 1.8em;
        }

        a.navbar-brand,
        a:hover{
            color: #009cff;
        }

        /* Blog */
        article.post-wrap .post-meta .post-meta-content a:hover,
        article.post-wrap .post-title h2.post-title a:hover,
        .sidebar .widget ul li a,
        .sidebar .widget.recent-posts-widget-with-thumbnails ul li a .rpwwt-post-title,
        .sidebar .widget.widget_rss ul li a.rsswidget,
        .single-post article.post-wrap .post-tag-pogon .share_social-pogon ul.share-social-icons li a:hover i,
        .single-post article.post-wrap .post_recommend .ova_blog .related-post .content .post-title a:hover,
        .content_comments .commentlists li.comment article.comment_item .comment-details .ova_reply .comment-reply-link,
        .content_comments .commentlists li.comment article.comment_item .comment-details .ova_reply .comment-edit-link,
        .content_comments .commentlists li.comment article.comment_item .comment-details .ova_reply i.fa-reply,
        .icon-before-heading .elementor-heading-title::before,
        article.post-wrap .post-meta .post-meta-content i
        {
            color: #009cff;
        }

        article.post-wrap .btn-pogon-default,
        .pagination-wrapper .blog_pagination .pagination li.active a,
        .pagination-wrapper .blog_pagination .pagination li a:hover,
        .single-post article.post-wrap .post-tag-pogon .post-tags-pogon > a
        {
            background-color: #009cff;
        }
        .content_comments .wrap_comment_form .comment-form .form-submit input[type=submit],
        .content_comments .commentlists .comment-respond .comment-form input[type=submit]
        {
            background-color: #009cff !important;
            border-color: #009cff !important;
        }



    </style>
    <link rel='stylesheet' id='ova-google-fonts-css'  href='//fonts.googleapis.com/css?family=Open+Sans%3A300%2Cregular%2C600%2C700%2C800' type='text/css' media='all' />
    <link rel='stylesheet' id='recent-posts-widget-with-thumbnails-public-style-css'  href={{ asset('wp-content/plugins/recent-posts-widget-with-thumbnails/public.css?ver=7.0.2') }} type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-css'  href={{ asset('wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.9.1') }} type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-animations-css'  href={{ asset('wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.0.16') }} type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-legacy-css'  href={{ asset('wp-content/plugins/elementor/assets/css/frontend-legacy.min.css?ver=3.0.16') }} type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css'  href={{ asset('wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.0.16') }} type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1535-css'  href={{ asset('wp-content/uploads/elementor/css/post-1535.css?ver=1610096704') }} type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-global-css'  href={{ asset('wp-content/uploads/elementor/css/global.css?ver=1610096704') }} type='text/css' media='all' />
    <link rel='stylesheet' id='style-elementor-css'  href={{ asset('wp-content/plugins/ova-framework/assets/css/style-elementor.css') }} type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;subset=vietnamese&#038;ver=5.6') }} type='text/css' media='all' />
    <script type='text/javascript' src={{ asset('wp-includes/js/jquery/jquery.min.js?ver=3.5.1') }} id='jquery-core-js'></script>
    <script type='text/javascript' src={{ asset('wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2') }} id='jquery-migrate-js'></script>
    <script type='text/javascript' src={{ asset('wp-content/plugins/wp-posts-carousel/owl.carousel/owl.carousel.js?ver=2.0.0') }} id='owl.carousel-js'></script>
    <script type='text/javascript' src={{ asset('wp-content/plugins/wp-posts-carousel/owl.carousel/jquery.mousewheel.min.js?ver=3.1.12') }} id='jquery-mousewheel-js'></script>
    <link rel="https://api.w.org/" href="https://honganh.vn/wp-json/" /><link rel="alternate" type="application/json" href="https://honganh.vn/wp-json/wp/v2/pages/3198" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://honganh.vn/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://honganh.vn/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.6" />
    <meta name="generator" content="WooCommerce 4.8.0" />
    <link rel='shortlink' href='https://honganh.vn/?p=3198' />
    <link rel="alternate" type="application/json+oembed" href="https://honganh.vn/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhonganh.vn%2Ftest-san-pham%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://honganh.vn/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhonganh.vn%2Ftest-san-pham%2F&#038;format=xml" />
    <script>var wp_posts_carousel_url="https://honganh.vn/wp-content/plugins/wp-posts-carousel/";</script>		<style>
        .hotline-phone-ring-circle {
            border-color: #009cff;
        }
        .hotline-phone-ring-circle-fill, .hotline-phone-ring-img-circle, .hotline-bar {
            background-color: #009cff;
        }
    </style>
    <style>
        .elementor-3198 .elementor-element.elementor-element-fecd65b:not(.elementor-motion-effects-element-type-background), .elementor-3198 .elementor-element.elementor-element-fecd65b > .elementor-motion-effects-container > .elementor-motion-effects-layer{background-image:url("https://honganh.vn/wp-content/uploads/2021/01/ktc-hero-ssd-a400-m2-lg.jpg");background-position:center center;background-repeat:no-repeat;background-size:cover;}.elementor-3198 .elementor-element.elementor-element-fecd65b{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:200px 0px 258px 0px;}.elementor-3198 .elementor-element.elementor-element-fecd65b > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-3198 .elementor-element.elementor-element-091df56 .cover_color{background-color:rgba(0,0,0,0.0);}.elementor-3198 .elementor-element.elementor-element-091df56 .ova_header_el .header_title{color:#fff;}.elementor-3198 .elementor-element.elementor-element-091df56 .ova_header_el .ovatheme_breadcrumbs ul.breadcrumb li{color:#fff;}.elementor-3198 .elementor-element.elementor-element-091df56 .ova_header_el .ovatheme_breadcrumbs ul.breadcrumb li a{color:#fff;}.elementor-3198 .elementor-element.elementor-element-091df56 .ova_header_el .ovatheme_breadcrumbs ul.breadcrumb a{color:#fff;}.elementor-3198 .elementor-element.elementor-element-091df56 .ova_header_el .ovatheme_breadcrumbs ul.breadcrumb .separator{color:#fff;}.elementor-3198 .elementor-element.elementor-element-091df56{text-align:center;}
    </style>


    <noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
    <style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style><script type="text/javascript">if (typeof ajaxurl === "undefined") {var ajaxurl = "https://honganh.vn/wp-admin/admin-ajax.php"}</script><link rel="icon" href="https://honganh.vn/wp-content/uploads/2019/03/cropped-photo_2020-12-03_16-02-38-2-32x32.png" sizes="32x32" />
    <link rel="icon" href="https://honganh.vn/wp-content/uploads/2019/03/cropped-photo_2020-12-03_16-02-38-2-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://honganh.vn/wp-content/uploads/2019/03/cropped-photo_2020-12-03_16-02-38-2-180x180.png" />
    <meta name="msapplication-TileImage" content="https://honganh.vn/wp-content/uploads/2019/03/cropped-photo_2020-12-03_16-02-38-2-270x270.png" />
    <style type="text/css" id="wp-custom-css">
        ul.elementor-icon-list-items .elementor-icon-list-icon i, ul.elementor-icon-list-items .elementor-icon-list-text a {
            color: white
        }

        .contact-header .elementor-widget-wrap {
            justify-content: flex-end;
        }



        .danh-muc-noi-bat .carousel-slider__item img {
            margin-bottom: 10px;
            margin-bottom: 10px;
            height: 140px;
            object-fit:cover;
        }
        .danh-muc-noi-bat .carousel-slider__item img:hover {
            -webkit-transform: translateY(-8px);
            transform: translateY(-8px);
        }

        .danh-muc-noi-bat .carousel-slider__item .carousel-slider__caption {
            margin-top: 13.33333333px;
            margin-bottom: 11px;
            line-height: 1.2em;
            height: 2.4em;
            text-transform: uppercase;
            text-align: center;
            overflow: hidden;
            position: relative;
        }

        .danh-muc-noi-bat .carousel-slider__item .carousel-slider__caption .caption{

            font-weight: 600;
            font-size: 16px;
            color: #828688;
        }
        .danh-muc-noi-bat .carousel-slider__item .carousel-slider__caption .caption:hover {
            color: grey;
        }

        .danh-muc-noi-bat .owl-item{
            display: block;
            padding: 0px 20px;
        }

        .btn-custom {
            font-size: 16px;
            font-weight: 600;
            background-color: #009cff;
            border-radius: 4px 4px 4px 4px;
            padding: 22px 33px 22px 33px;
        }

        .our-partners .owl-item {
            border: 0.1rem solid #c4cdd5;
        }


        .our-partners .carousel-slider__item {
            padding: 20px;
        }

        .image-hongAnh .elementor-element-populated {
            padding: 0!important;
        }

        .ova-heading .heading-title {
            font-size: 40px;
            font-family: "Open Sans"!important;
        }

        body *{
            font-family: "Open Sans";
        }

        .footer_link ul li:before {
            background: #009cff;
        }
        ul {
            list-style-type: disc;
        }

        article.post-wrap .post-title h2.post-title a,  article.post-wrap .post-title .post-title {
            word-break: break-word;
        }

        html {
            scroll-behavior: smooth;
        }

        .elementor-widget-counter .elementor-counter-title {
            font-family: "Open Sans";
        }

        .image-hongAnh img {
            min-width: 100%;
        }

        .linh-vuc-kinh-doanh .elementor-widget-ova_service, .linh-vuc-kinh-doanh .elementor-widget-ova_service .elementor-widget-container, .linh-vuc-kinh-doanh .elementor-widget-ova_service .elementor-widget-container .ova-service{
            height: 100%;
        }

        .linh-vuc-kinh-doanh .ova-service.two_column:hover {
            background-color: #009cff;
            border-color: #009cff !important;
        }

        #box-dich-vu .elementor-widget-ova_service, #box-dich-vu .elementor-widget-ova_service .elementor-widget-container, #box-dich-vu .elementor-widget-ova_service .elementor-widget-container .ova-service{
            height: 100%;
        }


        /*******************************
        * Does not work properly if "in" is added after "collapse".
        * Get free snippets on bootpen.com
        *******************************/
        .panel-group .panel {
            border-radius: 0;
            box-shadow: none;
            border-color: #EEEEEE;
        }

        .panel-default > .panel-heading {
            padding: 0;
            border-radius: 0;
            color: #212121;
            background-color: #FAFAFA;
            border-color: #EEEEEE;
        }

        .panel-title {
            font-size: 14px;
        }

        .panel-title > a {
            display: block;
            padding: 15px;
            text-decoration: none;
        }

        .more-less {
            float: right;
            color: #212121;
        }

        .panel-default > .panel-heading + .panel-collapse > .panel-body {
            border-top-color: #EEEEEE;
        }
        @font-face {
            font-family: 'Glyphicons Halflings';
            src: url('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.eot');
            src: url('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.eot?#iefix') format('embedded-opentype'),
            url('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.woff2') format('woff2'),
            url('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.woff') format('woff'),
            url('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.ttf') format('truetype'),
            url('//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular') format('svg');
        }

        .panel-title a.glyphicon-down::after {
            font-family: 'Glyphicons Halflings';
            /*     content: "\e113"; */
            content: "\e114";
            float: right;
            color: #003875;
        }
        .panel-title a.glyphicon-up::after {
            font-family: 'Glyphicons Halflings';
            content: "\e113";
            float: right;
            color: #003875;
        }

        .img-gioithieu img {
            width: 543px;
        }

        .footer_link ul li:before {
            content: unset;
        }

        .footer_link ul li {
            padding-left: 0;
        }

        .footer_link ul li a:hover, .pogon-blog .post_items .post_content h4 a:hover {
            color: #009CFF;
        }

        .no-padding .elementor-element-populated {
            padding: 0!important;
        }		</style>


    <style>
        .box-input_search{
            display: grid;
            grid-template-columns: 1fr;
            row-gap: 10px;
        }
        .box-filter {
            display: none;
        }

        @media (min-width: 768px) {
            .box-filter {
                display: block;
            }
            .res-mobile {
                display: none;
            }
        }
        @media (min-width: 992px) {
            .box-input_search{
                grid-template-columns: 1fr 100px;
                column-gap: 10px;
            }
        }

        @media (max-width: 576px) {
            .pagination {
                font-size: 10px;
            }
        }
        .link-action {
            text-decoration: none;
            color: black;
        }

        .link-action:hover {
            text-decoration: none;
        }

        .box-filter_items .link-action .box-filter_item {
            border-top: 1px solid #dee2e6;
            padding: 5px 10px;
        }

        .box-filter_items .link-action.active .box-filter_item, .box-filter_items .link-action .box-filter_item:hover {
            background: #009CFF;
            color: #ffffff;
        }
    </style>


</head>

<body class="page-template page-template-elementor_header_footer page page-id-3198 theme-pogon woocommerce-no-js yith-wcan-free elementor-default elementor-template-full-width elementor-kit-1535 elementor-page elementor-page-3198" ><div class="ova-wrapp">

    <div data-elementor-type="wp-post" data-elementor-id="1506" class="elementor elementor-1506" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-1323667 elementor-hidden-phone elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1323667" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-d1f2cb2" data-id="d1f2cb2" data-element_type="column">
                                <div class="elementor-column-wrap">
                                    <div class="elementor-widget-wrap">
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-fe2c52d" data-id="fe2c52d" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-3c0a6d5 contact-header elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="3c0a6d5" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ece0c91" data-id="ece0c91" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-61b1263 elementor-widget__width-auto elementor-widget elementor-widget-ova_contact_info" data-id="61b1263" data-element_type="widget" data-widget_type="ova_contact_info.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="ova_contact_us"><i class="icon  icon_phone"></i><div class="text"><a href="tel:+84946240000" >0946240000</a></div></div>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-e1146ed elementor-widget__width-auto elementor-widget elementor-widget-ova_contact_info" data-id="e1146ed" data-element_type="widget" data-widget_type="ova_contact_info.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="ova_contact_us"><i class="icon  icon_mail"></i><div class="text"><a href="mailto:info@honganh.vn" >info@honganh.vn</a></div></div>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="elementor-section elementor-top-section elementor-element elementor-element-ababfc0 elementor-section-content-middle ovamenu_shrink elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="ababfc0" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-b8dce58" data-id="b8dce58" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-af3ff02 elementor-widget elementor-widget-ova_logo" data-id="af3ff02" data-element_type="widget" data-widget_type="ova_logo.default">
                                            <div class="elementor-widget-container">

                                                <a class="ova_logo" href="https://honganh.vn/">

                                                    <img src="https://honganh.vn/wp-content/uploads/2019/03/photo_2020-12-03_16-02-38-2.png" alt="Công ty TNHH Máy tính Hồng Anh" class="desk-logo d-none d-xl-block" style="width:70px ; height:70px" />

                                                    <img src="https://honganh.vn/wp-content/uploads/2019/03/photo_2020-12-03_16-02-38-2.png" alt="Công ty TNHH Máy tính Hồng Anh" class="mobile-logo d-block d-xl-none" style="width:70px ; height:70px" />

                                                    <img src="https://honganh.vn/wp-content/uploads/2019/03/photo_2020-12-03_16-02-38-2.png" alt="Công ty TNHH Máy tính Hồng Anh" class="logo-fixed" style="width:70px ; height:70px" />
                                                </a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-158fdbd" data-id="158fdbd" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-e42ffda elementor-view-primary-menu elementor-widget__width-auto elementor-widget elementor-widget-ova_menu" data-id="e42ffda" data-element_type="widget" data-widget_type="ova_menu.default">
                                            <div class="elementor-widget-container">
                                                <div class="ova_menu_clasic">
                                                    <div class="ova_wrap_nav row NavBtn_left ">

                                                        <button class="ova_openNav" type="button">
                                                            <!-- <i class="fas fa-bars"></i> -->
                                                            <span class="bar">
							<span class="bar-menu-line"></span>
							<span class="bar-menu-line"></span>
							<span class="bar-menu-line"></span>
						</span>
                                                        </button>

                                                        <div class="ova_nav canvas_left canvas_bg_white">
                                                            <a href="javascript:void(0)" class="ova_closeNav"><i class="fas fa-times"></i></a>
                                                            <ul id="menu-primary-menu" class="menu sub_menu_dir_right"><li id="menu-item-900" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home menu-item-900"><a title="TRANG CHỦ" href="https://honganh.vn/"><span class="glyphicon 						"></span>&nbsp;TRANG CHỦ</a></li>
                                                                <li id="menu-item-22" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-22 dropdown"><a title="GIỚI THIỆU" href="https://honganh.vn/gioi-thieu/"><span class="glyphicon 						"></span>&nbsp;GIỚI THIỆU<i class="arrow_carrot-down"></i></a><button type="button" class="dropdown-toggle"><i class="arrow_carrot-down"></i></button>
                                                                    <ul role="menu" class=" dropdown-menu">
                                                                        <li id="menu-item-2793" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2793"><a title="Giới Thiệu" href="https://honganh.vn/gioi-thieu/">Giới Thiệu</a></li>
                                                                        <li id="menu-item-2794" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2794"><a title="Tại sao chọn Hồng Anh" href="https://honganh.vn/tai-sao-chon-hong-anh/">Tại sao chọn Hồng Anh</a></li>
                                                                        <li id="menu-item-2817" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2817"><a title="Quy Trình Hợp Tác" href="https://honganh.vn/quy-trinh-hop-tac/">Quy Trình Hợp Tác</a></li>
                                                                    </ul>
                                                                </li>
                                                                <li id="menu-item-23" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-23"><a title="DỊCH VỤ" href="https://honganh.vn/dich-vu/"><span class="glyphicon 						"></span>&nbsp;DỊCH VỤ</a></li>
                                                                <li id="menu-item-24" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-24 dropdown"><a title="CHÍNH SÁCH" href="#"><span class="glyphicon 						"></span>&nbsp;CHÍNH SÁCH<i class="arrow_carrot-down"></i></a><button type="button" class="dropdown-toggle"><i class="arrow_carrot-down"></i></button>
                                                                    <ul role="menu" class=" dropdown-menu">
                                                                        <li id="menu-item-1999" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1999"><a title="Quy Định Bảo Hành" href="https://honganh.vn/quy-dinh-bao-hanh/">Quy Định Bảo Hành</a></li>
                                                                        <li id="menu-item-2701" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2701"><a title="Quy Định Đổi Hàng" href="https://honganh.vn/quy-dinh-doi-hang/">Quy Định Đổi Hàng</a></li>
                                                                    </ul>
                                                                </li>
                                                                <li id="menu-item-3013" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3013"><a title="FAQs" href="https://honganh.vn/faqs/">FAQs</a></li>
                                                                <li id="menu-item-25" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-25"><a title="TIN TỨC" href="https://honganh.vn/tin-tuc/"><span class="glyphicon 						"></span>&nbsp;TIN TỨC</a></li>
                                                                <li id="menu-item-26" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-26"><a title="LIÊN HỆ" href="https://honganh.vn/lien-he"><span class="glyphicon 						"></span>&nbsp;LIÊN HỆ</a></li>
                                                            </ul>					</div>

                                                        <div class="ova_closeCanvas ova_closeNav"></div>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>
                                        <div class="elementor-element elementor-element-ed778e9 elementor-widget__width-auto elementor-hidden-phone elementor-widget elementor-widget-ova_search" data-id="ed778e9" data-element_type="widget" data-widget_type="ova_search.default">
                                            <div class="elementor-widget-container">
                                                <div class="wrap_search_pogon_popup">
                                                    <i class="ti-search"></i>
                                                    <div class="search_pogon_popup">
                                                        <span class="btn_close icon_close"></span>
                                                        <div class="container">
                                                            <form role="search" method="get" class="search-form" action="https://honganh.vn/">
                                                                <input type="search" class="search-field" placeholder="Search …" value="" name="s" title="Tìm kiếm cho:" />
                                                                <input type="submit" class="search-submit" value="Tìm kiếm" />
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <div data-elementor-type="wp-page" data-elementor-id="3198" class="elementor elementor-3198" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-fecd65b elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="fecd65b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-46fe5dc" data-id="46fe5dc" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <div class="elementor-element elementor-element-091df56 elementor-widget elementor-widget-ova_header_breadcrumbs" data-id="091df56" data-element_type="widget" data-widget_type="ova_header_breadcrumbs.default">
                                            <div class="elementor-widget-container">
                                                <!-- Display when you choose background per Post -->
                                                <div class="ova_header_el_bg">
                                                    <div class="cover_color"></div>
                                                </div>

                                                <div class="ova_header_el ">

                                                    <h1 class="second_font header_title">
                                                        Bảo hành					</h1>


                                                    <div class="ovatheme_breadcrumbs ovatheme_breadcrumbs_el">
                                                        <div id="breadcrumbs"><ul class="breadcrumb"><li><a href="https://honganh.vn/">Trang chủ</a></li> <li class="li_separator"><span class="separator"></span></li> <li>Bảo hành</li></ul></div>					</div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div class="container mb-3 mt-3">
            @foreach($data->products as $product)
                <div class="result-check-serial">
                    <div class="d-flex justify-content-center">
                        <table class="table" style="max-width: 700px;">
                            <tr>
                                <td><b>Serial</b>:</td>
                                <td>{{ $product->serial }}</td>
                            </tr>
                            <tr>
                                <td><b>Tên sản phẩm</b>:</td>
                                <td>{{ $product->name }}</td>
                            </tr>
                            <tr>
                                <td><b>Nhà cung cấp</b>:</td>
                                <td>{{ $product->serial }}</td>
                            </tr>
                            <tr>
                                <td><b>Ngày nhập</b>:</td>
                                <td>{{ $product->date_added }}</td>
                            </tr>
                            <tr>
                                <td><b>Bảo hành NCC</b>:</td>
                                <td>{{ $product->warranty_period_supplier }}</td>
                            </tr>
                            <tr>
                                <td><b>Tên khách hàng</b>:</td>
                                <td>{{ $product->customer_name }}</td>
                            </tr>
                            <tr>
                                <td><b>Ngày bán</b>:</td>
                                <td>{{ $product->date_of_sale }}</td>
                            </tr>
                            <tr>
                                <td><b>Bảo hành khi bán</b>:</td>
                                <td>{{ $product->warranty_period_sale }}</td>
                            </tr>
                            <tr>
                                <td><b>SĐT khách hàng</b>:</td>
                                <td>{{ $product->customer_phone }}</td>
                            </tr>
                            <tr>
                                <td><b>Email khách hàng</b>:</td>
                                <td>{{ $product->customer_email }}</td>
                            </tr>
                            <tr>
                                <td><b>Chi nhánh</b>:</td>
                                <td>{{ $product->branch }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div data-elementor-type="wp-post" data-elementor-id="145" class="elementor elementor-145" data-elementor-settings="[]">
        <div class="elementor-inner">
            <div class="elementor-section-wrap">
                <section class="elementor-section elementor-top-section elementor-element elementor-element-25be660 elementor-section-full_width elementor-section-height-default elementor-section-height-default" data-id="25be660" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-background-overlay"></div>
                    <div class="elementor-container elementor-column-gap-no">
                        <div class="elementor-row">
                            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-0536f83" data-id="0536f83" data-element_type="column">
                                <div class="elementor-column-wrap elementor-element-populated">
                                    <div class="elementor-widget-wrap">
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-a4a4df0 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="a4a4df0" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-48921d9" data-id="48921d9" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-1edcaca elementor-widget elementor-widget-ova_logo" data-id="1edcaca" data-element_type="widget" data-widget_type="ova_logo.default">
                                                                    <div class="elementor-widget-container">

                                                                        <a class="ova_logo" href="https://honganh.vn/">

                                                                            <img src="https://honganh.vn/wp-content/uploads/2019/03/Hong-Anh-computer-logo.png" alt="Công ty TNHH Máy tính Hồng Anh" class="desk-logo d-none d-xl-block" style="width:192px ; height:147px" />

                                                                            <img src="https://honganh.vn/wp-content/uploads/2019/03/Hong-Anh-computer-logo.png" alt="Công ty TNHH Máy tính Hồng Anh" class="mobile-logo d-block d-xl-none" style="width:100px ; height:100px" />

                                                                            <img src="https://honganh.vn/wp-content/uploads/2019/03/photo_2020-12-03_16-02-38-2.png" alt="Công ty TNHH Máy tính Hồng Anh" class="logo-fixed" style="width:70px ; height:70px" />
                                                                        </a>

                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-40ac470 footer_intro elementor-widget elementor-widget-text-editor" data-id="40ac470" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix"><p style="color: #ffffff;">Đối tác tin cậy trong lĩnh vực kinh doanh máy tính và giải pháp tư vấn lắp đặt phòng game.</p></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-115c840" data-id="115c840" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-3e15758 elementor-widget elementor-widget-heading" data-id="3e15758" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h4 class="elementor-heading-title elementor-size-default">DỊCH VỤ</h4>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-9f5c79d footer_link elementor-widget elementor-widget-text-editor" data-id="9f5c79d" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-text-editor elementor-clearfix"><ul><li><a href="#">Bán buôn linh kiện máy tính</a></li><li><a href="#">Tư vấn lắp đặt phòng game</a></li><li><a href="#">Cung Cấp Giải Pháp Workstation</a></li><li><a href="#">Xây dựng hệ thống giám sát an ninh </a></li><li><a href="#">Triển khai lắp đặt máy chủ</a></li></ul></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-881eb4b" data-id="881eb4b" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-c0841ab elementor-widget elementor-widget-heading" data-id="c0841ab" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h4 class="elementor-heading-title elementor-size-default">TIN TỨC MỚI NHẤT</h4>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-2e8e63d elementor-widget elementor-widget-ova_blog_footer" data-id="2e8e63d" data-element_type="widget" data-widget_type="ova_blog_footer.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="pogon-blog">


                                                                            <div class="post_items">
                                                                                <div class="img-feature">
                                                                                    <img src="https://honganh.vn/wp-content/uploads/2021/01/13-Toan-Dung-Vinh-Phuc-300x235.jpg" srcset="https://honganh.vn/wp-content/uploads/2021/01/13-Toan-Dung-Vinh-Phuc-300x235.jpg 300w, https://honganh.vn/wp-content/uploads/2021/01/13-Toan-Dung-Vinh-Phuc-scaled-600x469.jpg 600w, https://honganh.vn/wp-content/uploads/2021/01/13-Toan-Dung-Vinh-Phuc-1024x801.jpg 1024w, https://honganh.vn/wp-content/uploads/2021/01/13-Toan-Dung-Vinh-Phuc-768x601.jpg 768w, https://honganh.vn/wp-content/uploads/2021/01/13-Toan-Dung-Vinh-Phuc-1536x1201.jpg 1536w, https://honganh.vn/wp-content/uploads/2021/01/13-Toan-Dung-Vinh-Phuc-2048x1602.jpg 2048w" sizes="(max-width: 600px) 100vw, 600px" alt="Hãng AMD &#038; Hồng Anh tổng kết sự kiện tặng 50 triệu cho các đại lý bán sỉ">
                                                                                </div>
                                                                                <div class="post_content">
                                                                                    <h4><a href="https://honganh.vn/tin-tuc/tong-ket-su-kien-hang-amd-tang-50-trieu-cho-cac-dai-ly-ban-si-hong-anh/" title="Hãng AMD &#038; Hồng Anh tổng kết sự kiện tặng 50 triệu cho các đại lý bán sỉ">Hãng AMD &#038; Hồng Anh tổng kết</a></h4>
                                                                                    <span class="post-date">
							7 Tháng Một, 2021						</span>
                                                                                </div>
                                                                            </div>


                                                                            <div class="post_items">
                                                                                <div class="img-feature">
                                                                                    <img src="https://honganh.vn/wp-content/uploads/2020/12/06-MBC-computer-Hanoi-300x225.jpg" srcset="https://honganh.vn/wp-content/uploads/2020/12/06-MBC-computer-Hanoi-300x225.jpg 300w, https://honganh.vn/wp-content/uploads/2020/12/06-MBC-computer-Hanoi-600x450.jpg 600w, https://honganh.vn/wp-content/uploads/2020/12/06-MBC-computer-Hanoi-1024x767.jpg 1024w, https://honganh.vn/wp-content/uploads/2020/12/06-MBC-computer-Hanoi-768x575.jpg 768w, https://honganh.vn/wp-content/uploads/2020/12/06-MBC-computer-Hanoi.jpg 1276w" sizes="(max-width: 600px) 100vw, 600px" alt="Chương trình hỗ trợ các đại lý bán sỉ Hồng Anh trị giá 2 triệu từ hãng AMD">
                                                                                </div>
                                                                                <div class="post_content">
                                                                                    <h4><a href="https://honganh.vn/tin-tuc/chuong-trinh-ho-tro-cac-dai-ly-ban-si-hong-anh-tri-gia-2-trieu-tu-hang-amd/" title="Chương trình hỗ trợ các đại lý bán sỉ Hồng Anh trị giá 2 triệu từ hãng AMD">Chương trình hỗ trợ các đại lý</a></h4>
                                                                                    <span class="post-date">
							21 Tháng Mười Hai, 2020						</span>
                                                                                </div>
                                                                            </div>


                                                                            <div class="post_items">
                                                                                <div class="img-feature">
                                                                                    <img src="https://honganh.vn/wp-content/uploads/2019/03/image1-48-1024x679-1-300x199.jpg" srcset="https://honganh.vn/wp-content/uploads/2019/03/image1-48-1024x679-1-300x199.jpg 300w, https://honganh.vn/wp-content/uploads/2019/03/image1-48-1024x679-1-600x398.jpg 600w, https://honganh.vn/wp-content/uploads/2019/03/image1-48-1024x679-1-768x509.jpg 768w, https://honganh.vn/wp-content/uploads/2019/03/image1-48-1024x679-1.jpg 1024w" sizes="(max-width: 600px) 100vw, 600px" alt="Tất tần tật về bo mạch chủ 500-series mới sắp ra mắt của Intel">
                                                                                </div>
                                                                                <div class="post_content">
                                                                                    <h4><a href="https://honganh.vn/tin-tuc/tat-tan-tat-ve-bo-mach-chu-500-series-moi-sap-ra-mat-cua-intel/" title="Tất tần tật về bo mạch chủ 500-series mới sắp ra mắt của Intel">Tất tần tật về bo mạch chủ</a></h4>
                                                                                    <span class="post-date">
							18 Tháng Mười Hai, 2020						</span>
                                                                                </div>
                                                                            </div>


                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-9901a6e" data-id="9901a6e" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-2044061 elementor-widget elementor-widget-heading" data-id="2044061" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h4 class="elementor-heading-title elementor-size-default">LIÊN HỆ</h4>		</div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-e2096df elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="e2096df" data-element_type="widget" data-widget_type="icon-list.default">
                                                                    <div class="elementor-widget-container">
                                                                        <ul class="elementor-icon-list-items">
                                                                            <li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-map-marker-alt"></i>						</span>
                                                                                <span class="elementor-icon-list-text"><a href="https://goo.gl/maps/UYXxSZx9wP3SURfa7" target="_blank">Số 61 Ngõ 342 Khương Đình, Thanh Xuân, Hà Nội</a></span>
                                                                            </li>
                                                                            <li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-map-marker-alt"></i>						</span>
                                                                                <span class="elementor-icon-list-text"><a href="https://goo.gl/maps/BDgjXEDPketrJaqz7" target="_blank">77/32 Nhất Chi Mai, Phường 13, Q. Tân Bình, Tp. Hồ Chí Minh</a></span>
                                                                            </li>
                                                                            <li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-map-marker-alt"></i>						</span>
                                                                                <span class="elementor-icon-list-text"><a href="https://goo.gl/maps/zeKeJPtrhoTDsEHu5" target="_blank">Số 54 Tống Duy Tân , Q. Liên Chiểu , TP Đà Nẵng.</a></span>
                                                                            </li>
                                                                            <li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-phone-alt"></i>						</span>
                                                                                <span class="elementor-icon-list-text"><a href="tel:+84946240000">0946240000</a></span>
                                                                            </li>
                                                                            <li class="elementor-icon-list-item">
											<span class="elementor-icon-list-icon">
							<i aria-hidden="true" class="fas fa-mail-bulk"></i>						</span>
                                                                                <span class="elementor-icon-list-text"><a href="mailto:info@honganh.vn">info@honganh.vn</a></span>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-2f71d0f elementor-widget elementor-widget-ova_social" data-id="2f71d0f" data-element_type="widget" data-widget_type="ova_social.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="ova_social ova-framework-social-icons-wrapper">
                                                                            <div class="content">

                                                                                <a class="ova-framework-icon ova-framework-social-icon ova-framework-social-icon-facebook" style=";" href="https://www.facebook.com/HongAnhComputer.Official" target="_blank">
                                                                                    <span class="ova-framework-screen-only">Facebook</span>
                                                                                    <i class="fa fa-facebook" style=""></i>
                                                                                </a>

                                                                                <a class="ova-framework-icon ova-framework-social-icon ova-framework-social-icon-linkedin" style=";" href="https://www.linkedin.com/company/honganhcomputer" target="_blank">
                                                                                    <span class="ova-framework-screen-only">Linkedin</span>
                                                                                    <i class="fa fa-linkedin" style=""></i>
                                                                                </a>

                                                                                <a class="ova-framework-icon ova-framework-social-icon ova-framework-social-icon-instagram" style=";" href="https://www.instagram.com/honganhcomputer/" target="_blank">
                                                                                    <span class="ova-framework-screen-only">Instagram</span>
                                                                                    <i class="fa fa-instagram" style=""></i>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-01464b5 elementor-widget elementor-widget-html" data-id="01464b5" data-element_type="widget" data-widget_type="html.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="fb-page" style="width:100%;" data-href="https://www.facebook.com/HongAnhComputer.Official" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false"><blockquote cite="https://www.facebook.com/HongAnhComputer.Official" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HongAnhComputer.Official">Hồng Anh Computer - Đơn vị cung cấp linh kiện giá buôn</a></blockquote></div>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section class="elementor-section elementor-inner-section elementor-element elementor-element-3f37e5f elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="3f37e5f" data-element_type="section">
                                            <div class="elementor-container elementor-column-gap-default">
                                                <div class="elementor-row">
                                                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d80e564" data-id="d80e564" data-element_type="column">
                                                        <div class="elementor-column-wrap elementor-element-populated">
                                                            <div class="elementor-widget-wrap">
                                                                <div class="elementor-element elementor-element-19b18c0 elementor-widget elementor-widget-heading" data-id="19b18c0" data-element_type="widget" data-widget_type="heading.default">
                                                                    <div class="elementor-widget-container">
                                                                        <span class="elementor-heading-title elementor-size-default">Copyright © 2020 Công Ty TNHH Máy Tính Hồng Anh</span>		</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

</div> <!-- Ova Wrapper -->

<div id='fb-root'></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v6.0&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class='fb-customerchat'
     attribution='wordpress'
     attribution_version='1.8'
     page_id=211317936265512
>
</div>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v9.0" nonce="smKQ6tMy"></script>		<div class="hotline-phone-ring-wrap">
    <div class="hotline-phone-ring">
        <div class="hotline-phone-ring-circle"></div>
        <div class="hotline-phone-ring-circle-fill"></div>
        <div class="hotline-phone-ring-img-circle">
            <a href="tel:84946240000" class="pps-btn-img">
                <img src="https://honganh.vn/wp-content/plugins/hotline-phone-ring/assets/images/icon-2.png" alt="Số điện thoại" width="50" />
            </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    (function () {
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
    })()
</script>
<link rel='stylesheet' id='elementor-post-1506-css'  href={{ asset('https://honganh.vn/wp-content/uploads/elementor/css/post-1506.css?ver=1610096704') }} type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-145-css'  href={{ asset('https://honganh.vn/wp-content/uploads/elementor/css/post-145.css?ver=1610096705') }} type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-shared-0-css'  href={{ asset('https://honganh.vn/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.12.0') }} type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-solid-css'  href=" {{ asset('css/solid.min.css') }}" type='text/css' media='all' />
<script type='text/javascript' id='contact-form-7-js-extra'>
    /* <![CDATA[ */
    var wpcf7 = {"apiSettings":{"root":"https:\/\/honganh.vn\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
</script>
<script type='text/javascript' src={{ asset('wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.3.2') }} id='contact-form-7-js'></script>
<script type='text/javascript' src={{ asset('wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js?ver=2.70') }} id='jquery-blockui-js'></script>
<script type='text/javascript' src={{ asset('wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js?ver=4.8.0') }} id='wc-add-to-cart-js'></script>
<script type='text/javascript' src={{ asset('wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js?ver=2.1.4') }} id='js-cookie-js'></script>

<script type='text/javascript' src={{ asset('wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js?ver=4.8.0') }} id='wc-cart-fragments-js'></script>
<script type='text/javascript' src={{ asset('wp-includes/js/jquery/ui/effect.min.js?ver=1.12.1') }} id='jquery-effects-core-js'></script>
<script type='text/javascript' src={{ asset('wp-content/themes/pogon/assets/libs/bootstrap/js/bootstrap.bundle.min.js') }} id='bootstrap-js'></script>
<script type='text/javascript' src="{{ asset('js/script.js') }}" id='pogon-script-js'></script>
<script type='text/javascript' src={{ asset('wp-includes/js/wp-embed.min.js?ver=5.6') }} id='wp-embed-js'></script>
<script type='text/javascript' src={{ asset('wp-content/plugins/ova-framework/assets/js/script-elementor.js?ver=5.6') }} id='script-elementor-js'></script>
<script type='text/javascript' src={{ asset('wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.0.16') }} id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src={{ asset('wp-includes/js/jquery/ui/core.min.js?ver=1.12.1') }} id='jquery-ui-core-js'></script>
<script type='text/javascript' src={{ asset('wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.8.1') }} id='elementor-dialog-js'></script>
<script type='text/javascript' src={{ asset('wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2') }} id='elementor-waypoints-js'></script>
<script type='text/javascript' src={{ asset('wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6') }} id='swiper-js'></script>
<script type='text/javascript' src={{ asset('wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.0.16') }} id='share-link-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
    var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Thu ph\u00f3ng","share":"Share","playVideo":"Ch\u01a1i Video","previous":"Previous","next":"Next","close":"\u0110\u00f3ng"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"3.0.16","is_static":false,"legacyMode":{"elementWrappers":true},"urls":{"assets":"https:\/\/honganh.vn\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":3198,"title":"Test%20San%20Pham%20-%20C%C3%B4ng%20ty%20TNHH%20M%C3%A1y%20t%C3%ADnh%20H%E1%BB%93ng%20Anh","excerpt":"","featuredImage":false}};
</script>
<script type='text/javascript' src={{ asset('wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.0.16') }} id='elementor-frontend-js'></script>

</body><!-- /body -->
</html>
